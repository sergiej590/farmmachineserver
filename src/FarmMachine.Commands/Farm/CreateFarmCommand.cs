﻿using FarmMachine.Commands.Base;
using FarmMachine.Dto.Farm;

namespace FarmMachine.Commands.Farm
{
    public class CreateFarmCommand : Command
    {
        public FarmDto Model { get; set; }
        public string UserId { get; set; }

        public CreateFarmCommand(FarmDto model, string userId)
        {
            Model = model;
            UserId = userId;
        }
    }
}
