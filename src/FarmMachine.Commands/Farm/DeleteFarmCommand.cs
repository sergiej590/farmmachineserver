﻿using System;
using FarmMachine.Commands.Base;

namespace FarmMachine.Commands.Farm
{
    public class DeleteFarmCommand: Command
    {
        public string UserId { get; set; }
        public Guid FarmId { get; set; }

        public DeleteFarmCommand(Guid farmId, string userId)
        {
            FarmId = farmId;
            UserId = userId;
        }
    }
}
