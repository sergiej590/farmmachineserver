﻿using System;
using FarmMachine.Commands.Base;
using FarmMachine.Dto.Farm;

namespace FarmMachine.Commands.Farm
{
    public class UpdateFarmCommand:Command
    {
        public Guid FarmId { get; set; }    
        public FarmDto Model { get; set; }
        public string UserId { get; set; }

        public UpdateFarmCommand(Guid farmId, FarmDto model, string userId)
        {
            FarmId = farmId;
            Model = model;
            UserId = userId;
        }
    }
}
