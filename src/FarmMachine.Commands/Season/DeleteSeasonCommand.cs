﻿using System;
using FarmMachine.Commands.Base;

namespace FarmMachine.Commands.Season
{
    public class DeleteSeasonCommand : Command
    {
        public string UserId { get; set; }
        public Guid SeasonId { get; set; }

        public DeleteSeasonCommand(Guid seasonId, string userId)
        {
            SeasonId = seasonId;
            UserId = userId;
        }
    }
}
