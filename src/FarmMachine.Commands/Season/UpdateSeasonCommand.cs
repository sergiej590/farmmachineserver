﻿using System;
using FarmMachine.Commands.Base;
using FarmMachine.Dto.Season;

namespace FarmMachine.Commands.Season
{
    public class UpdateSeasonCommand:Command
    {
        public Guid SeasonId { get; set; }
        public SeasonDto Model { get; set; }
        public string UserId { get; set; }

        public UpdateSeasonCommand(Guid seasonId, SeasonDto model, string userId)
        {
            SeasonId = seasonId;
            Model = model;
            UserId = userId;
        }
    }
}
