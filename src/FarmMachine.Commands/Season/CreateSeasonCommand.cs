﻿using FarmMachine.Commands.Base;
using FarmMachine.Dto.Season;

namespace FarmMachine.Commands.Season
{
    public class CreateSeasonCommand:Command
    {
        public SeasonDto Model { get; set; }
        public string UserId { get; set; }

        public CreateSeasonCommand(SeasonDto model, string userId)
        {
            Model = model;
            UserId = userId;
        }
    }
}
