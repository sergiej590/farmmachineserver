﻿using FarmMachine.Commands.Base;
using System;

namespace FarmMachine.Commands.User
{
    public class RegisterFacebookUserCommand : Command
    {
        public string AccessToken { get; set; }

        public RegisterFacebookUserCommand(Guid id, int version, string accessToken) 
            : base(id, version)
        {
            AccessToken = accessToken;
        }
    }
}
