﻿using System;

namespace FarmMachine.Commands.Base
{
    public class Command : ICommand
    {
        public Guid Id { get; private set; }
        public int Version { get; private set; }

        public Command()
        {
            Id = Guid.NewGuid();
            Version = -1;
        }
        public Command(Guid id, int version)
        {
            Id = id;
            Version = version;
        }
    }
}
