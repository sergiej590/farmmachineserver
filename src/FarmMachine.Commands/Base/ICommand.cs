﻿using System;

namespace FarmMachine.Commands.Base
{
    public interface ICommand
    {
        Guid Id { get; }
    }
}
