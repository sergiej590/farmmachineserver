﻿using System;
using FarmMachine.Commands.Base;

namespace FarmMachine.Commands.DefaultUserInfo
{
    public class UpdateDefaultUserInfoCommand : Command
    {
        public UpdateDefaultUserInfoCommand(string userId, Guid? farmId, Guid? seasonId)
        {
            UserId = userId;
            FarmId = farmId;
            SeasonId = seasonId;
        }

        public string UserId { get; set; }
        public Guid? FarmId { get; set; }
        public Guid? SeasonId { get; set; }
    }
}
