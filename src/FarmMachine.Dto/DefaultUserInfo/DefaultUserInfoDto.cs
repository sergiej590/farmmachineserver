﻿using FarmMachine.Dto.Base;
using System;

namespace FarmMachine.Dto.DefaultUserInfo
{
    public class DefaultUserInfoDto : IQueryResult
    {
        public DefaultUserInfoDto(Guid? farmId, Guid? seasonId)
        {
            FarmId = farmId;
            SeasonId = seasonId;
        }
        public Guid? FarmId { get; set; }
        public Guid? SeasonId { get; set; }
    }
}
