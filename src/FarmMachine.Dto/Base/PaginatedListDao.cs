﻿using System.Collections.Generic;

namespace FarmMachine.Dto.Base
{
    public class PaginatedListDto<T> : ListDto<T>
    {
        public int TotalRows { get; set; }

        public PaginatedListDto(IEnumerable<T> items):base(items)
        {
        }
    }
}
