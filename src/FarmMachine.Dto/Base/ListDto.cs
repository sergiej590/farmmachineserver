﻿using System.Collections.Generic;

namespace FarmMachine.Dto.Base
{
    public class ListDto<T> :IQueryResult
    {
        public IEnumerable<T> Items { get; set; }

        public ListDto(IEnumerable<T> items)
        {
            Items = items;
        }
    }
}
