﻿using System;
using FarmMachine.Dto.Base;

namespace FarmMachine.Dto.Season
{
    public class SimpleSeasonDto : IQueryResult
    {
        public Guid Id { get; set; }
        public DateTimeOffset DateFrom { get; set; }
        public DateTimeOffset DateTo { get; set; }
        public DateTimeOffset CreationTime { get; set; }
        public DateTimeOffset ModificationTime { get; set; }

        public int FarmCount { get; set; }
        public int Days { get; set; }

        public SimpleSeasonDto(Guid id, DateTimeOffset dateFrom, DateTimeOffset dateTo, int farmCount, int days, DateTimeOffset creationTime, DateTimeOffset modificationTime)
        {
            Id = id;
            DateFrom = dateFrom;
            DateTo = dateTo;
            FarmCount = farmCount;
            Days = days;
            CreationTime = creationTime;
            ModificationTime = modificationTime;
        }
    }
}
