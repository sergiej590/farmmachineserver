﻿using System;
using System.Collections.Generic;
using FarmMachine.Dto.Farm;

namespace FarmMachine.Dto.Season
{
    public class SeasonDto : SimpleSeasonDto
    {
        public IEnumerable<SimpleFarmDto> Farms { get; set; }

        public SeasonDto(Guid id, DateTimeOffset dateFrom, DateTimeOffset dateTo, int farmCount, int days, DateTimeOffset creationTime, DateTimeOffset modificationTime) : base(id, dateFrom, dateTo,farmCount, days, creationTime, modificationTime)
        {
        }
    }
}
