﻿namespace FarmMachine.Dto
{
    public class UserDto
    {
        public string AccessToken { get; set; }
    }
}
