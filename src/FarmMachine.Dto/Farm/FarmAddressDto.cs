﻿using System;
using FarmMachine.Dto.Base;

namespace FarmMachine.Dto.Farm
{
    public class FarmAddressDto : IQueryResult
    {
        public FarmAddressDto(Guid? id, string country, string postCode, string city, string street, string homeNo, bool main)
        {
            Id = id;
            Country = country;
            PostCode = postCode;
            City = city;
            Street = street;
            HomeNo = homeNo;
            Main = main;
        }

        public Guid? Id { get; set; }
        public string Country { get; set; }
        public string PostCode { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        public string HomeNo { get; set; }
        public bool Main { get; set; }
    }
}
