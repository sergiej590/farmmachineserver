﻿using System;
using FarmMachine.Dto.Base;

namespace FarmMachine.Dto.Farm
{
    public class FarmCategoryDto:IQueryResult
    {
        public FarmCategoryDto(Guid id, string name, bool itemChecked)
        {
            Id = id;
            Name = name;
            Checked = itemChecked;
        }

        public Guid Id { get; set; }
        public string Name { get; set; }
        public bool Checked { get; set; }
    }
}
