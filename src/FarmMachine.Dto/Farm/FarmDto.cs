﻿using System;
using System.Collections.Generic;

namespace FarmMachine.Dto.Farm
{
    public class FarmDto : SimpleFarmDto
    {
        public IEnumerable<FarmCategoryDto> Categories { get; set; }

        public IEnumerable<FarmAddressDto> Addresses { get; set; }

        public FarmDto(Guid id, string name, string identifier,bool active,
            DateTimeOffset creationTime, DateTimeOffset modificationTime,
            IEnumerable<FarmCategoryDto> categories, IEnumerable<FarmAddressDto> addresses)
                :base(id, name,identifier, active ,creationTime, modificationTime)
        {
            Categories = categories;
            Addresses = addresses;
        }
    }
}
