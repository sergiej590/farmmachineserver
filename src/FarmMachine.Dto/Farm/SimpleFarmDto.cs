﻿using System;
using FarmMachine.Dto.Base;

namespace FarmMachine.Dto.Farm
{
    public class SimpleFarmDto : IQueryResult
    {
        public SimpleFarmDto(Guid id, string name, string identifier, bool active,
            DateTimeOffset creationTime, DateTimeOffset modificationTime)
        {
            Id = id;
            Name = name;
            Identifier = identifier;
            Active = active;
            CreationTime = creationTime;
            ModificationTime = modificationTime;
        }

        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Identifier { get; set; }
        public bool Active { get; set; }
        public DateTimeOffset CreationTime { get; set; }
        public DateTimeOffset ModificationTime { get; set; }
        public bool Checked { get; set; }
    }
}
