﻿using FarmMachine.Queries.Base;

namespace FarmMachine.Queries.Farm
{
    public class GetFarmsQuery : IQuery
    {
        public GetFarmsQuery(string userId)
        {
            UserId = userId;
        }
        public string UserId { get; set; }
    }
}
