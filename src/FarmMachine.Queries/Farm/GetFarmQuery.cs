﻿using System;
using FarmMachine.Queries.Base;

namespace FarmMachine.Queries.Farm
{
    public class GetFarmQuery : IQuery
    {
        public GetFarmQuery(Guid id, string userId)
        {
            Id = id;
            UserId = userId;
        }

        public Guid Id { get; set; }
        public string UserId { get; set; }
    }
}
