﻿using FarmMachine.Queries.Base;

namespace FarmMachine.Queries.DefaultUserInfo
{
    public class GetDefaultUserInfoQuery : IQuery
    {
        public GetDefaultUserInfoQuery(string userId)
        {
            UserId = userId;
        }

        public string UserId { get; set; }
    }
}
