﻿using System;
using FarmMachine.Queries.Base;

namespace FarmMachine.Queries.Season
{
    public class GetSeasonsQuery:IQuery
    {
        public string UserId { get; set; }
        public DateTimeOffset? DateFrom { get; set; }
        public DateTimeOffset? DateTo { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }

        public GetSeasonsQuery(string userId, DateTimeOffset? dateFrom, DateTimeOffset? dateTo, int page, int pageSize) 
        {
            UserId = userId;
            DateFrom = dateFrom;
            DateTo = dateTo;
            Page = page;
            PageSize = pageSize;
        }
    }
}
