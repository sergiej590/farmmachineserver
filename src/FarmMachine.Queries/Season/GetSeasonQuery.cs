﻿using System;
using FarmMachine.Queries.Base;

namespace FarmMachine.Queries.Season
{
    public class GetSeasonQuery:IQuery
    {
        public string UserId { get; set; }
        public Guid Id { get; set; }

        public GetSeasonQuery(string userId, Guid id)
        {
            UserId = userId;
            Id = id;
        }
    }
}
