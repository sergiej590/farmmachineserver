﻿using FarmMachine.Dto.Base;
using FarmMachine.Messaging.Exceptions;
using FarmMachine.Messaging.Factories;
using FarmMachine.Queries.Base;
using Microsoft.Extensions.Logging;

namespace FarmMachine.Messaging.Dispatchers
{
    public class QueryDispatcher : IQueryDispatcher
    {
        private readonly IQueryHandlerFactory queryHandlerFactory;
        private readonly ILogger logger;

        public QueryDispatcher(IQueryHandlerFactory queryHandlerFactory,
                               ILogger<QueryDispatcher> logger)
        {
            this.queryHandlerFactory = queryHandlerFactory;
            this.logger = logger;
        }
        public TResult Dispatch<TParameter, TResult>(TParameter query) where TParameter : IQuery where TResult : IQueryResult
        {
            var handler = queryHandlerFactory.GetHandler<TParameter, TResult>();

            if (handler == null)
            {
                logger.LogError($"no query handler registered for type [{typeof(TParameter)}, {typeof(TResult)}]");
                throw new UnregisteredDomainQueryException($"no query handler registered for type [{typeof(TParameter)}, {typeof(TResult)}]");
            }

            return handler.Retrieve(query);
        }
    }
}
