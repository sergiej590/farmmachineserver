﻿using FarmMachine.Dto.Base;
using FarmMachine.Queries.Base;

namespace FarmMachine.Messaging.Dispatchers
{
    public interface IQueryDispatcher
    {
        TResult Dispatch<TParameter, TResult>(TParameter query)
            where TParameter : IQuery
            where TResult : IQueryResult;
    }
}
