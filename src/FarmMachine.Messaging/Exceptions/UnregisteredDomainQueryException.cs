﻿using System;

namespace FarmMachine.Messaging.Exceptions
{
    public class UnregisteredDomainQueryException : Exception
    {
        public UnregisteredDomainQueryException(string message) : base(message) { }
    }
}
