﻿using FarmMachine.Commands.Base;
using FarmMachine.Messaging.Exceptions;
using FarmMachine.Messaging.Factories;
using Microsoft.Extensions.Logging;

namespace FarmMachine.Messaging.Busses
{
    public class InMemoryCommandBus : ICommandBus
    {
        private readonly ICommandHandlerFactory commandHandlerFactory;
        private readonly ILogger logger;

        public InMemoryCommandBus(ICommandHandlerFactory commandHandlerFactory,
                                  ILogger<InMemoryCommandBus> logger)
        {
            this.commandHandlerFactory = commandHandlerFactory;
            this.logger = logger;
        }

        public void Send<T>(T command) where T : Command
        {
            var handler = commandHandlerFactory.GetHandler<T>();

            if (handler == null)
            {
                logger.LogError($"no command handler registered for type {typeof(T)}");
                throw new UnregisteredDomainCommandException($"no command handler registered for type {typeof(T)}");
            }

            handler.Execute(command);
        }
    }
}
