﻿using FarmMachine.Commands.Base;

namespace FarmMachine.Messaging.Busses
{
    public interface ICommandBus
    {
        void Send<T>(T command) where T : Command;
    }
}
