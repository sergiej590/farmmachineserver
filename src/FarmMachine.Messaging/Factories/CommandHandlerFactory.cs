﻿using System;
using System.Security.Cryptography;
using Autofac;
using FarmMachine.CommandHandlers.Handlers.Base;
using FarmMachine.Commands.Base;
using Microsoft.Extensions.Logging;

namespace FarmMachine.Messaging.Factories
{
    public class CommandHandlerFactory : ICommandHandlerFactory
    {
        private readonly ILifetimeScope lifetimeScope;
        private readonly ILogger logger;

        public CommandHandlerFactory(ILifetimeScope lifetimeScope,
                                     ILogger<CommandHandlerFactory> logger)
        {
            this.lifetimeScope = lifetimeScope;
            this.logger = logger;
        }
        public ICommandHandler<T> GetHandler<T>() where T : Command
        {
            try
            {
                var handler = lifetimeScope.Resolve<ICommandHandler<T>>();

                return handler;
            }
            catch(Exception ex)
            {
                logger.LogError($"type [{typeof(T)}] was not resolved by lifetimeScope", ex);
                return null;
            }
        }
    }
}
