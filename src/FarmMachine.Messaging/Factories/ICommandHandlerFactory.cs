﻿using FarmMachine.CommandHandlers.Handlers.Base;
using FarmMachine.Commands.Base;

namespace FarmMachine.Messaging.Factories
{
    public interface ICommandHandlerFactory
    {
        ICommandHandler<T> GetHandler<T>() where T : Command;
    }
}
