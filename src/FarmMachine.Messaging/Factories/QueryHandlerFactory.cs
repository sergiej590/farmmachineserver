﻿using System;
using FarmMachine.Dto.Base;
using FarmMachine.Queries.Base;
using Autofac;
using FarmMachine.QueryHandlers.Handlers.Base;
using Microsoft.Extensions.Logging;

namespace FarmMachine.Messaging.Factories
{
    public class QueryHandlerFactory : IQueryHandlerFactory
    {
        private readonly ILifetimeScope lifetimeScope;
        private readonly ILogger<QueryHandlerFactory> logger;

        public QueryHandlerFactory(ILifetimeScope lifetimeScope,
                                   ILogger<QueryHandlerFactory> logger)
        {
            this.lifetimeScope = lifetimeScope;
            this.logger = logger;
        }

        public IQueryHandler<TParameter, TResult> GetHandler<TParameter, TResult>() where TParameter : IQuery
            where TResult : IQueryResult
        {
            try
            {
                var handler = lifetimeScope.Resolve<IQueryHandler<TParameter, TResult>>();

                return handler;
            }
            catch (Exception ex)
            {
                logger.LogError($"type [{typeof(TParameter)}, {typeof(TResult)}] was not resolved by lifetimeScope", ex);
                return null;
            }
        }
    }
}
