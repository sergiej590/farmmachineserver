﻿using FarmMachine.Dto.Base;
using FarmMachine.Queries.Base;
using FarmMachine.QueryHandlers.Handlers.Base;

namespace FarmMachine.Messaging.Factories
{
    public interface IQueryHandlerFactory
    {
        IQueryHandler<TParameter, TResult> GetHandler<TParameter, TResult>()
            where TParameter : IQuery
            where TResult : IQueryResult;
    }
}
