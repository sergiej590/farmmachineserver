﻿using System.Threading.Tasks;

namespace FarmMachine.Services.Interfaces
{
    public interface IRestService
    {
        Task<T> Get<T>(string url) where T : class;
    }
}
