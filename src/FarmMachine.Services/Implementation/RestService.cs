﻿using System;
using FarmMachine.Services.Interfaces;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace FarmMachine.Services.Implementation
{
    public class RestService : IRestService
    {
        private readonly ILogger<RestService> logger;

        public RestService(ILogger<RestService> logger)
        {
            this.logger = logger;
        }

        public async Task<T> Get<T>(string url) where T: class
        {
            try
            {
                var client = new HttpClient();
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("User-Agent", ".NET Foundation Repository Reporter");

                var contentResponse = await client.GetStringAsync(url);

                return JsonConvert.DeserializeObject<T>(contentResponse);
            }
            catch(Exception ex)
            {
                logger.LogError($"error during requesting [{url}]", ex);
                return null;
            }
        }
    }
}

