﻿using System.Collections.Generic;
using AutoMapper;
using FarmMachine.Dto.Base;
using FarmMachine.Dto.Season;
using FarmMachine.Models.Entities;

namespace FarmMachine.QueryHandlers.Mappings
{
    public class SeasonProfile : Profile
    {
        public SeasonProfile()
        {
            CreateMap<Season, SimpleSeasonDto>()
                .ConstructUsing(m => new SimpleSeasonDto(m.Id, m.DateFrom, m.DateTo, m.FarmCount, m.Days, m.CreationTime, m.ModificationTime));

            CreateMap<Season, SeasonDto>()
               .ForMember(m => m.Farms, m => m.Ignore())
               .ConstructUsing(m => new SeasonDto(m.Id, m.DateFrom, m.DateTo, m.FarmCount, m.Days, m.CreationTime, m.ModificationTime));

            CreateMap<IEnumerable<SimpleSeasonDto>, PaginatedListDto<SimpleSeasonDto>>()
             .ConstructUsing(m => new PaginatedListDto<SimpleSeasonDto>(m));
        }
    }
}
