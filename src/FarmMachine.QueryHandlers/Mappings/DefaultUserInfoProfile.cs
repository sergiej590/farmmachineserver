﻿using AutoMapper;
using FarmMachine.Dto.DefaultUserInfo;
using FarmMachine.Models.Entities;

namespace FarmMachine.QueryHandlers.Mappings
{
    public class DefaultUserInfoProfile : Profile
    {
        public DefaultUserInfoProfile()
        {
            CreateMap<DefaultUserInfo, DefaultUserInfoDto>()
                .ConstructUsing(m => new DefaultUserInfoDto(m.FarmId, m.SeasonId));
        }
    }
}
