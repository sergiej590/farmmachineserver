﻿using System.Collections.Generic;
using AutoMapper;
using FarmMachine.Dto.Base;
using FarmMachine.Dto.Farm;
using FarmMachine.Models.Entities;

namespace FarmMachine.QueryHandlers.Mappings
{
    public class FarmProfile : Profile
    {
        public FarmProfile()
        {
            CreateMap<Farm, SimpleFarmDto>()
                .ConstructUsing(m => new SimpleFarmDto(m.Id, m.Name, m.FarmIdentifier, m.Active, m.CreationTime,m.ModificationTime));

            CreateMap<Farm, FarmDto>()
                .ForMember(m => m.Addresses, m => m.Ignore())
                .ForMember(m => m.Categories, m => m.Ignore())
                .ConstructUsing(m => new FarmDto(m.Id, m.Name, m.FarmIdentifier,m.Active, m.CreationTime,m.ModificationTime, null,null));

            CreateMap<IEnumerable<SimpleFarmDto>, ListDto<SimpleFarmDto>>()
               .ConstructUsing(m => new ListDto<SimpleFarmDto>(m));
            
            CreateMap<FarmCategory, FarmCategoryDto>()
                .ConstructUsing(m => new FarmCategoryDto(m.Id, m.Name, false));

            CreateMap<IEnumerable<FarmCategoryDto>, ListDto<FarmCategoryDto>>()
               .ConstructUsing(m => new ListDto<FarmCategoryDto>(m));

            CreateMap<FarmAddress, FarmAddressDto>()
                .ConstructUsing(m => new FarmAddressDto(m.Id, m.Country,m.PostCode,m.City,m.Street,m.HomeNo,m.Main));
        }
    }
}
