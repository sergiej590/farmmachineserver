﻿using FarmMachine.Dto.Base;
using FarmMachine.Queries.Base;

namespace FarmMachine.QueryHandlers.Handlers.Base
{
        public interface IQueryHandler<in TParameter, out TResult> where TResult : IQueryResult where TParameter : IQuery
        {
            TResult Retrieve(TParameter query);
        }
    
}
