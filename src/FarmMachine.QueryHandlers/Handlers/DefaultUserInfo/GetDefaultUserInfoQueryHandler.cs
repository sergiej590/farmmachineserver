﻿using AutoMapper;
using FarmMachine.Dto.DefaultUserInfo;
using FarmMachine.Queries.DefaultUserInfo;
using FarmMachine.QueryHandlers.Handlers.Base;
using FarmMachine.Storage.Repositories;
using Microsoft.Extensions.Logging;

namespace FarmMachine.QueryHandlers.Handlers.DefaultUserInfo
{
    public class GetDefaultUserInfoQueryHandler : IQueryHandler<GetDefaultUserInfoQuery, DefaultUserInfoDto>
    {
        private readonly IRepository<Models.Entities.DefaultUserInfo> defaultUserInfoRepository;
        private readonly IMapper mapper;
        private readonly ILogger<GetDefaultUserInfoQueryHandler> logger;

        public GetDefaultUserInfoQueryHandler(IRepository<Models.Entities.DefaultUserInfo> defaultUserInfoRepository,
            IMapper mapper, ILogger<GetDefaultUserInfoQueryHandler> logger)
        {
            this.defaultUserInfoRepository = defaultUserInfoRepository;
            this.mapper = mapper;
            this.logger = logger;
        }
        public DefaultUserInfoDto Retrieve(GetDefaultUserInfoQuery query)
        {
            var entity = defaultUserInfoRepository.Get(f => f.CreatorUserId == query.UserId);

            if (entity == null)
            {
                return null;
            }

            var defaultUserInfo = mapper.Map<DefaultUserInfoDto>(entity);
            return defaultUserInfo;
        }
    }
}