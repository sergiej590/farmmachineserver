﻿using System.Linq;
using AutoMapper;
using FarmMachine.Dto.Base;
using FarmMachine.Dto.Farm;
using FarmMachine.Models.Entities;
using FarmMachine.Queries.Farm;
using FarmMachine.QueryHandlers.Handlers.Base;
using FarmMachine.Storage.Repositories;

namespace FarmMachine.QueryHandlers.Handlers.Farm
{
    public class GetFarmCategoriesQueryHandler : IQueryHandler<GetFarmCategoriesQuery, ListDto<FarmCategoryDto>>
    {
        private readonly IRepository<FarmCategory> farmCategoryRepository;
        private readonly IMapper mapper;

        public GetFarmCategoriesQueryHandler(IRepository<FarmCategory> farmCategoryRepository,
            IMapper mapper)
        {
            this.farmCategoryRepository = farmCategoryRepository;
            this.mapper = mapper;
        }
        public ListDto<FarmCategoryDto> Retrieve(GetFarmCategoriesQuery query)
        {
            var entity = farmCategoryRepository.List();
            return mapper.Map<ListDto<FarmCategoryDto>>(entity.Select(mapper.Map<FarmCategoryDto>));
        }
    }
}
