﻿using System.Linq;
using AutoMapper;
using FarmMachine.Dto.Base;
using FarmMachine.Dto.Farm;
using FarmMachine.Queries.Farm;
using FarmMachine.QueryHandlers.Handlers.Base;
using FarmMachine.Storage.Repositories;

namespace FarmMachine.QueryHandlers.Handlers.Farm
{
    public class GetFarmsQueryHandler : IQueryHandler<GetFarmsQuery, ListDto<SimpleFarmDto>>
    {
        private readonly IRepository<Models.Entities.Farm> farmRepository;
        private readonly IMapper mapper;

        public GetFarmsQueryHandler(IRepository<Models.Entities.Farm> farmRepository,
            IMapper mapper)
        {
            this.farmRepository = farmRepository;
            this.mapper = mapper;
        }
        public ListDto<SimpleFarmDto> Retrieve(GetFarmsQuery query)
        {
            var entity = farmRepository.List(f => f.CreatorUserId == query.UserId && !f.Deleted);

            return mapper.Map<ListDto<SimpleFarmDto>>(entity.Select(mapper.Map<SimpleFarmDto>));
        }
    }
}
