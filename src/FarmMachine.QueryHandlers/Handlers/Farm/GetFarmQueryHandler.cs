﻿using System.Linq;
using AutoMapper;
using FarmMachine.Dto.Farm;
using FarmMachine.Queries.Farm;
using FarmMachine.QueryHandlers.Handlers.Base;
using FarmMachine.Storage.Extensions;
using FarmMachine.Storage.Repositories;
using Microsoft.Extensions.Logging;

namespace FarmMachine.QueryHandlers.Handlers.Farm
{
    public class GetFarmQueryHandler : IQueryHandler<GetFarmQuery, FarmDto>
    {
        private readonly IRepository<Models.Entities.Farm> farmRepository;
        private readonly IMapper mapper;
        private readonly ILogger<GetFarmQueryHandler> logger;

        public GetFarmQueryHandler(IRepository<Models.Entities.Farm> farmRepository,
            IMapper mapper, ILogger<GetFarmQueryHandler> logger)
        {
            this.farmRepository = farmRepository;
            this.mapper = mapper;
            this.logger = logger;
        }
        public FarmDto Retrieve(GetFarmQuery query)
        {
            var entity = farmRepository.GetFarm(f => f.Id == query.Id);

            if (entity == null)
            {
                return null;
            }

            if (entity.CreatorUserId != query.UserId)
            {
                logger.LogError($"user [id:{query.UserId}] is not allowed to request for farm [id:{query.Id}");
                return null;
            }
            
            if (entity.Deleted)
            {
                logger.LogError($"farm [id:{query.Id}] is already deleted");
                return null;
            }

            var farm =  mapper.Map<FarmDto>(entity);
            farm.Addresses = entity.Addresses.Select(mapper.Map<FarmAddressDto>);
            farm.Categories = entity.FarmCategories.Select(fc => new FarmCategoryDto(fc.FarmCategoryId, null, true));

            return farm;
        }
    }
}
