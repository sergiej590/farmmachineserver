﻿using System;
using System.Linq;
using System.Linq.Expressions;
using AutoMapper;
using FarmMachine.Dto.Base;
using FarmMachine.Dto.Season;
using FarmMachine.Queries.Season;
using FarmMachine.QueryHandlers.Handlers.Base;
using FarmMachine.Storage.Repositories;

namespace FarmMachine.QueryHandlers.Handlers.Season
{
    public class GetSeasonsQueryHandler : IQueryHandler<GetSeasonsQuery, PaginatedListDto<SimpleSeasonDto>>
    {
        private readonly IRepository<Models.Entities.Season> seasonRepository;
        private readonly IMapper mapper;

        public GetSeasonsQueryHandler(IRepository<Models.Entities.Season> seasonRepository,
            IMapper mapper)
        {
            this.seasonRepository = seasonRepository;
            this.mapper = mapper;
        }
        public PaginatedListDto<SimpleSeasonDto> Retrieve(GetSeasonsQuery query)
        {
            var expression = GetExpression(query);
            
            var entities = seasonRepository.PaginatedList(expression, s => s.CreationTime, query.Page, query.PageSize);
            int totalRows = seasonRepository.Count(expression);
            
            var result = mapper.Map<PaginatedListDto<SimpleSeasonDto>>(entities.Select(mapper.Map<SimpleSeasonDto>));
            result.TotalRows = totalRows;

            return result;
        }

        private Expression<Func<Models.Entities.Season, bool>> GetExpression(GetSeasonsQuery query)
        {
            Func<Models.Entities.Season, bool> exp = s => s.CreatorUserId == query.UserId && !s.Deleted;

            if (query.DateFrom.HasValue)
            {
                var exp1 = exp;
                exp = s => exp1(s) && s.DateTo >= query.DateFrom;
            }

            if (query.DateTo.HasValue)
            {
                var exp1 = exp;
                exp = s => exp1(s) && s.DateFrom <= query.DateTo;
            }

            return s => exp(s);
        }
    }
}
