﻿using System;
using AutoMapper;
using FarmMachine.Dto.Farm;
using FarmMachine.Dto.Season;
using FarmMachine.Queries.Season;
using FarmMachine.QueryHandlers.Handlers.Base;
using FarmMachine.Storage.Repositories;
using Microsoft.Extensions.Logging;
using FarmMachine.Storage.Extensions;
using System.Linq;

namespace FarmMachine.QueryHandlers.Handlers.Season
{
    public class GetSeasonQueryHandler : IQueryHandler<GetSeasonQuery, SeasonDto>
    {
        private readonly IRepository<Models.Entities.Season> seasonRepository;
        private readonly IMapper mapper;
        private readonly ILogger<GetSeasonQueryHandler> logger;

        public GetSeasonQueryHandler(IRepository<Models.Entities.Season> seasonRepository,
            IMapper mapper, ILogger<GetSeasonQueryHandler> logger)
        {
            this.seasonRepository = seasonRepository;
            this.mapper = mapper;
            this.logger = logger;
        }
        public SeasonDto Retrieve(GetSeasonQuery query)
        {
            var entity = seasonRepository.GetSeason(f => f.Id == query.Id);

            if (entity == null)
            {
                return null;
            }

            if (entity.CreatorUserId != query.UserId)
            {
                logger.LogError($"user [id:{query.UserId}] is not allowed to request for season [id:{query.Id}");
                return null;
            }

            if (entity.Deleted)
            {
                logger.LogError($"season [id:{query.Id}] is already deleted");
                return null;
            }

            var season = mapper.Map<SeasonDto>(entity);
            season.Farms = entity.Farms.Select(fc => new SimpleFarmDto(fc.FarmId, null, null, false, DateTimeOffset.MinValue, DateTimeOffset.MinValue));

            return season;
        }
    }
}
