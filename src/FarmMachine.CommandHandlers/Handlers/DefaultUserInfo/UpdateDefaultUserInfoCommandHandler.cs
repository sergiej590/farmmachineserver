﻿using FarmMachine.CommandHandlers.Exceptions;
using FarmMachine.CommandHandlers.Factories;
using FarmMachine.CommandHandlers.Handlers.Base;
using FarmMachine.Commands.DefaultUserInfo;
using FarmMachine.Models.Entities;
using FarmMachine.Storage.Repositories;
using Microsoft.Extensions.Logging;

namespace FarmMachine.CommandHandlers.Handlers.DefaultUserInfo
{
    public class UpdateDefaultUserInfoCommandHandler : ICommandHandler<UpdateDefaultUserInfoCommand>
    {
        private readonly IRepository<Models.Entities.DefaultUserInfo> defaultUserInfoRepository;
        private readonly ILogger<UpdateDefaultUserInfoCommandHandler> logger;
        private readonly IRepository<ActionNotification> actionNotificationRepository;
        private readonly IActionNotificationFactory actionNotificationFactory;

        public UpdateDefaultUserInfoCommandHandler(
            IRepository<Models.Entities.DefaultUserInfo> defaultUserInfoRepository,
            ILogger<UpdateDefaultUserInfoCommandHandler> logger,
            IRepository<ActionNotification> actionNotificationRepository,
            IActionNotificationFactory actionNotificationFactory)
        {
            this.defaultUserInfoRepository = defaultUserInfoRepository;
            this.logger = logger;
            this.actionNotificationRepository = actionNotificationRepository;
            this.actionNotificationFactory = actionNotificationFactory;
        }
        public void Execute(UpdateDefaultUserInfoCommand command)
        {
            ExecuteAction(command);
        }

        private void ExecuteAction(UpdateDefaultUserInfoCommand command)
        {
            var entity = defaultUserInfoRepository.Get(f => f.CreatorUserId == command.UserId);

            if (entity == null)
            {
                logger.LogError($"default user info [user id:{command.UserId}] does not exist");
                throw new UserValidationException("FARM_NOT_EXIST");
            }

            entity.FarmId = command.FarmId;
            entity.SeasonId = command.SeasonId;

            var notification = actionNotificationFactory.Create(Models.Enums.ActionNotifyTypes.DefaultUserInfoUpdated, null, command.UserId, entity.Id);
            actionNotificationRepository.Insert(notification);

            defaultUserInfoRepository.SaveChanges();
        }
    }
}
