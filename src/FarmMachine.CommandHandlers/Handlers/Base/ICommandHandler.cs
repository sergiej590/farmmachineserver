﻿using FarmMachine.Commands.Base;

namespace FarmMachine.CommandHandlers.Handlers.Base
{
    public interface ICommandHandler<in TCommand> where TCommand : Command
    {
        void Execute(TCommand command);
    }
}
