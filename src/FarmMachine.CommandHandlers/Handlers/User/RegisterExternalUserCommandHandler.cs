﻿using System;
using System.Threading.Tasks;
using FarmMachine.CommandHandlers.Exceptions;
using FarmMachine.CommandHandlers.Handlers.Base;
using FarmMachine.Commands.User;
using FarmMachine.Models.Entities;
using FarmMachine.Models.Models;
using FarmMachine.Services.Interfaces;
using FarmMachine.Storage.Repositories;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace FarmMachine.CommandHandlers.Handlers.User
{
    public class RegisterExternalUserCommandHandler : ICommandHandler<RegisterFacebookUserCommand>
    {
        private readonly UserManager<ApplicationUser> userManager;
        private readonly IRepository<Models.Entities.DefaultUserInfo> defaultUserInfoRepository;
        private readonly IRestService restService;
        private readonly IConfigurationRoot configuration;
        private readonly ILogger<RegisterExternalUserCommandHandler> logger;

        public RegisterExternalUserCommandHandler(UserManager<ApplicationUser> userManager,
                                                  IRepository<Models.Entities.DefaultUserInfo> defaultUserInfoRepository,
                                                  IRestService restService,
                                                  IConfigurationRoot configuration,
                                                  ILogger<RegisterExternalUserCommandHandler> logger)
        {
            this.userManager = userManager;
            this.defaultUserInfoRepository = defaultUserInfoRepository;
            this.restService = restService;
            this.configuration = configuration;
            this.logger = logger;
        }

        public void Execute(RegisterFacebookUserCommand command)
        {
            Validate(command);

            ExecuteAsync(command).Wait();
        }

        private async Task ExecuteAsync(RegisterFacebookUserCommand command)
        {
            var urlFacebook =
                $"{configuration["TokenAuthentication:ExternalProviders:Facebook:GetUserInfoUrl"]}{command.AccessToken}";
            var facebookUser = await restService.Get<FacebookUser>(urlFacebook);

            if (facebookUser == null)
            {
                logger.LogError("Access token was not authenticated by Facebook");
                throw new Exception("Access token was not authenticated by Facebook");
            }

            logger.LogInformation($"user authenticated by facebook [email: {facebookUser.Email}]");

            var user = new ApplicationUser {UserName = facebookUser.Email, Email = facebookUser.Email};
            var result = await userManager.CreateAsync(user);

            if (result.Succeeded)
            {
                var userInfo = new UserLoginInfo("Facebook", facebookUser.Id,
                    $"{facebookUser.First_Name} {facebookUser.Last_Name}");
                result = await userManager.AddLoginAsync(user, userInfo);

                if (!result.Succeeded)
                {
                    logger.LogError($"Error during registering user [email: {facebookUser.Email}]");
                    throw new Exception($"Error during registering user [email: {facebookUser.Email}]");
                }

                var defaultUserInfo = new Models.Entities.DefaultUserInfo
                {
                    Id = Guid.NewGuid(),
                    CreatorUserId = user.Id
                };
                defaultUserInfoRepository.Insert(defaultUserInfo);

                logger.LogInformation($"user registered [email: {facebookUser.Email}]");
            }
        }

        private void Validate(RegisterFacebookUserCommand command)
        {
            if (string.IsNullOrEmpty(command.AccessToken))
            {
                logger.LogError($"{nameof(command.AccessToken)} is empty");
                throw new UserValidationException("REG_FB_USER_VAL_ACCESSTOKEN_EMPTY");
            }
        }
    }
}
