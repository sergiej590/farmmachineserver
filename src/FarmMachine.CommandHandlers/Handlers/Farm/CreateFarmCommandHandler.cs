﻿using FarmMachine.Commands.Farm;
using System;
using System.Collections.Generic;
using System.Linq;
using FarmMachine.CommandHandlers.Exceptions;
using FarmMachine.CommandHandlers.Handlers.Base;
using FarmMachine.Dto.Farm;
using FarmMachine.Models.Entities;
using FarmMachine.Storage.Repositories;
using Microsoft.Extensions.Logging;
using FarmMachine.CommandHandlers.Factories;

namespace FarmMachine.CommandHandlers.Handlers.Farm
{
    public class CreateFarmCommandHandler : ICommandHandler<CreateFarmCommand>
    {
        private readonly IRepository<Models.Entities.Farm> farmRepository;
        private readonly IRepository<FarmCategoryManyRelation> farmCategoryRepository;
        private readonly ILogger<CreateFarmCommandHandler> logger;
        private readonly IActionNotificationFactory actionNotificationFactory;
        private readonly IRepository<ActionNotification> actionNotificationRepository;
        private readonly IRepository<Models.Entities.DefaultUserInfo> defaultUserInfoRepository;

        public CreateFarmCommandHandler(IRepository<Models.Entities.Farm> farmRepository,
            IRepository<FarmCategoryManyRelation> farmCategoryRepository,
            IRepository<ActionNotification> actionNotificationRepository,
            IRepository<Models.Entities.DefaultUserInfo> defaultUserInfoRepository,
            ILogger<CreateFarmCommandHandler> logger,
            IActionNotificationFactory actionNotificationFactory)
        {
            this.farmRepository = farmRepository;
            this.farmCategoryRepository = farmCategoryRepository;
            this.logger = logger;
            this.actionNotificationFactory = actionNotificationFactory;
            this.actionNotificationRepository = actionNotificationRepository;
            this.defaultUserInfoRepository = defaultUserInfoRepository;
        }
        public void Execute(CreateFarmCommand command)
        {
            InitModel(command);
            Validate(command);
            ExecuteAction(command);
        }

        private void InitModel(CreateFarmCommand command)
        {
            command.Model.Id = Guid.NewGuid();
            command.Model.Active = true;
            command.Model.CreationTime = DateTimeOffset.Now;
            command.Model.ModificationTime = DateTimeOffset.Now;
            command.Model.Categories = command.Model.Categories.Where(c => c.Checked);
        }

        private void ExecuteAction(CreateFarmCommand command)
        {
            var farm = CreateFarm(command);
            farmRepository.Insert(farm);

            var notification = actionNotificationFactory.Create(Models.Enums.ActionNotifyTypes.FarmCreated, farm, command.UserId,farm.Id);
            actionNotificationRepository.Insert(notification);

            farmRepository.SaveChanges();
        }

        private Models.Entities.Farm CreateFarm(CreateFarmCommand command)
        {

        var farm = new Models.Entities.Farm
            {
                Id = command.Model.Id,
                Name = command.Model.Name,
                FarmIdentifier = command.Model.Identifier,
                Active = command.Model.Active,
                CreationTime = command.Model.CreationTime,
                ModificationTime = command.Model.ModificationTime,
                CreatorUserId = command.UserId,
                Addresses = new List<FarmAddress>()
            };

            foreach (var addressModel in command.Model.Addresses)
            {
                addressModel.Id = Guid.NewGuid();
                var address = new FarmAddress
                {
                    Id = addressModel.Id.Value,
                    Country = addressModel.Country,
                    City = addressModel.City,
                    PostCode = addressModel.PostCode,
                    Street = addressModel.Street,
                    HomeNo = addressModel.HomeNo,
                    Main = addressModel.Main
                };
                farm.Addresses.Add(address);
            }

            foreach (var categoryModel in command.Model.Categories)
            {
                var category = new FarmCategoryManyRelation
                {
                    Id = Guid.NewGuid(),
                    Farm = farm,
                    FarmCategoryId = categoryModel.Id
                };
                farmCategoryRepository.Insert(category);
            }

            var defaultUserInfo = defaultUserInfoRepository.Get(d => d.CreatorUserId == command.UserId && !d.FarmId.HasValue);
            if (defaultUserInfo != null)
            {
                defaultUserInfo.Farm = farm;
            }

            return farm;
        }

        private void Validate(CreateFarmCommand command)
        {
            ValidateFarm(command);

            foreach (var address in command.Model.Addresses)
            {
                ValidateAddress(address);
            }

            if (farmRepository.Any(f => f.CreatorUserId == command.UserId &&
                                        (f.Name == command.Model.Name ||
                                         f.FarmIdentifier == command.Model.Identifier)))
            {
                throw new UserValidationException("CREATE_FARM_VAL_FARM_EXIST");
            }
        }

        private void ValidateFarm(CreateFarmCommand command)
        {
            if (string.IsNullOrEmpty(command.Model.Name))
            {
                logger.LogError($"{nameof(command.Model.Name)} is empty");
                throw new UserValidationException("CREATE_FARM_VAL_NAME_EMPTY");
            }

            if (string.IsNullOrEmpty(command.Model.Identifier))
            {
                logger.LogError($"{nameof(command.Model.Identifier)} is empty");
                throw new UserValidationException("CREATE_FARM_VAL_IDENTIFIER_EMPTY");
            }
        }

        private void ValidateAddress(FarmAddressDto address)
        {
            if (string.IsNullOrEmpty(address.Country))
            {
                logger.LogError($"{nameof(address.Country)} is empty");
                throw new UserValidationException("CREATE_FARM_VAL_ADDRESS_COUNTRY_EMPTY");
            }

            if (string.IsNullOrEmpty(address.City))
            {
                logger.LogError($"{nameof(address.City)} is empty");
                throw new UserValidationException("CREATE_FARM_VAL_ADDRESS_CITY_EMPTY");
            }

            if (string.IsNullOrEmpty(address.Country))
            {
                logger.LogError($"{nameof(address.PostCode)} is empty");
                throw new UserValidationException("CREATE_FARM_VAL_ADDRESS_POSTCODE_EMPTY");
            }

            if (string.IsNullOrEmpty(address.Country))
            {
                logger.LogError($"{nameof(address.Street)} is empty");
                throw new UserValidationException("CREATE_FARM_VAL_ADDRESS_STREET_EMPTY");
            }
        }
    }
}
