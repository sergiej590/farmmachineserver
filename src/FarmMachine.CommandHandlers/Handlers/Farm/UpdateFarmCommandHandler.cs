﻿using System;
using System.Linq;
using FarmMachine.CommandHandlers.Exceptions;
using FarmMachine.CommandHandlers.Factories;
using FarmMachine.CommandHandlers.Handlers.Base;
using FarmMachine.Commands.Farm;
using FarmMachine.Dto.Farm;
using FarmMachine.Models.Entities;
using FarmMachine.Storage.Extensions;
using FarmMachine.Storage.Repositories;
using Microsoft.Extensions.Logging;

namespace FarmMachine.CommandHandlers.Handlers.Farm
{
    public class UpdateFarmCommandHandler: ICommandHandler<UpdateFarmCommand>
    {
        private readonly IRepository<Models.Entities.Farm> farmRepository;
        private readonly IRepository<Models.Entities.DefaultUserInfo> defaultUserInfoRepository;
        private readonly IRepository<FarmCategoryManyRelation> farmCategoryRepository;
        private readonly ILogger<UpdateFarmCommandHandler> logger;
        private readonly IActionNotificationFactory actionNotificationFactory;
        private readonly IRepository<ActionNotification> actionNotificationRepository;

        public UpdateFarmCommandHandler(IRepository<Models.Entities.Farm> farmRepository,
            IRepository<Models.Entities.DefaultUserInfo> defaultUserInfoRepository,
            IRepository<FarmCategoryManyRelation> farmCategoryRepository,
            ILogger<UpdateFarmCommandHandler> logger,
            IRepository<ActionNotification> actionNotificationRepository,
            IActionNotificationFactory actionNotificationFactory)
        {
            this.farmRepository = farmRepository;
            this.defaultUserInfoRepository = defaultUserInfoRepository;
            this.farmCategoryRepository = farmCategoryRepository;
            this.logger = logger;
            this.actionNotificationFactory = actionNotificationFactory;
            this.actionNotificationRepository = actionNotificationRepository;
        }
        public void Execute(UpdateFarmCommand command)
        {
            InitModel(command);
            Validate(command);
            ExecuteAction(command);
        }

        private void InitModel(UpdateFarmCommand command)
        {
            command.Model.Id = command.FarmId;
            command.Model.ModificationTime = DateTimeOffset.Now;
            command.Model.Categories = command.Model.Categories.Where(c => c.Checked);
        }

        private void ExecuteAction(UpdateFarmCommand command)
        {
            var entity = farmRepository.GetFarm(f => f.Id == command.FarmId);

            if (entity == null)
            {
                logger.LogError($"farm [id:{command.FarmId}] does not exist");
                throw new UserValidationException("FARM_NOT_EXIST");
            }

            if (entity.CreatorUserId != command.UserId)
            {
                logger.LogError($"user [id:{command.UserId}] is not allowed to update farm [id:{command.FarmId}");
                throw new UserValidationException("USER_NOT_ALLOWED_TO_UPDATE");
            }

            if (entity.Deleted)
            {
                logger.LogError($"farm [id:{command.FarmId}] is already deleted");
                throw new UserValidationException("FARM_DELETED");
            }

            UpdateFarm(command, entity);

            var notification = actionNotificationFactory.Create(Models.Enums.ActionNotifyTypes.FarmUpdated, entity, command.UserId, entity.Id);
            actionNotificationRepository.Insert(notification);

            farmRepository.SaveChanges();
        }

        private void UpdateFarm(UpdateFarmCommand command, Models.Entities.Farm entity)
        {
            entity.FarmIdentifier = command.Model.Identifier;
            entity.Name = command.Model.Name;
            entity.ModificationTime = command.Model.ModificationTime;
            entity.Active = command.Model.Active;

            foreach (var addressModel in command.Model.Addresses)
            {
                var address = entity.Addresses.SingleOrDefault(a => a.Id == addressModel.Id);

                if (address != null)
                {
                    address.Country = addressModel.Country;
                    address.City = addressModel.City;
                    address.PostCode = addressModel.PostCode;
                    address.Street = addressModel.Street;
                    address.HomeNo = addressModel.HomeNo;
                    address.Main = addressModel.Main;
                }
                else
                {
                    addressModel.Id = Guid.NewGuid();
                    address = new FarmAddress
                    {
                        Id = addressModel.Id.Value,
                        Country = addressModel.Country,
                        City = addressModel.City,
                        PostCode = addressModel.PostCode,
                        Street = addressModel.Street,
                        HomeNo = addressModel.HomeNo,
                        Main = addressModel.Main
                    };
                    entity.Addresses.Add(address);
                }
            }

            var addressesToDelete = entity.Addresses.Where(a => command.Model.Addresses.All(am => am.Id != a.Id)).ToList();
            foreach (var addressToDelete in addressesToDelete)
            {
                entity.Addresses.Remove(addressToDelete);
            }

            
            foreach (var categoryModel in command.Model.Categories)
            {
                var category = entity.FarmCategories.SingleOrDefault(c => c.FarmCategoryId == categoryModel.Id);

                if (category == null)
                {
                    category = new FarmCategoryManyRelation
                    {
                        Id = Guid.NewGuid(),
                        Farm = entity,
                        FarmCategoryId = categoryModel.Id
                    };
                    farmCategoryRepository.Insert(category);
                }
            }

            var categoriesToDelete =
                entity.FarmCategories.Where(
                    c => command.Model.Categories.All(cm => cm.Id != c.FarmCategoryId)).ToList();
            foreach (var categoryToDelete in categoriesToDelete)
            {
                entity.FarmCategories.Remove(categoryToDelete);
            }

            if (!command.Model.Active)
            {
                var defaultUserInfo = defaultUserInfoRepository.Get(d => d.CreatorUserId == command.UserId && d.FarmId == command.FarmId);
                if (defaultUserInfo != null)
                {
                    defaultUserInfo.FarmId = null;
                }
            }
        }

        private void Validate(UpdateFarmCommand command)
        {
            ValidateFarm(command);

            foreach (var address in command.Model.Addresses)
            {
                ValidateAddress(address);
            }

            if (farmRepository.Any(f => f.CreatorUserId == command.UserId &&
                                        f.Id != command.FarmId &&
                                        (f.Name == command.Model.Name ||
                                         f.FarmIdentifier == command.Model.Identifier)))
            {
                throw new UserValidationException("UPDATE_FARM_VAL_FARM_EXIST");
            }
        }

        private void ValidateFarm(UpdateFarmCommand command)
        {
            if (string.IsNullOrEmpty(command.Model.Name))
            {
                logger.LogError($"{nameof(command.Model.Name)} is empty");
                throw new UserValidationException("UPDATE_FARM_VAL_NAME_EMPTY");
            }

            if (string.IsNullOrEmpty(command.Model.Identifier))
            {
                logger.LogError($"{nameof(command.Model.Identifier)} is empty");
                throw new UserValidationException("UPDATE_FARM_VAL_IDENTIFIER_EMPTY");
            }
        }

        private void ValidateAddress(FarmAddressDto address)
        {
            if (string.IsNullOrEmpty(address.Country))
            {
                logger.LogError($"{nameof(address.Country)} is empty");
                throw new UserValidationException("UPDATE_FARM_VAL_ADDRESS_COUNTRY_EMPTY");
            }

            if (string.IsNullOrEmpty(address.City))
            {
                logger.LogError($"{nameof(address.City)} is empty");
                throw new UserValidationException("UPDATE_FARM_VAL_ADDRESS_CITY_EMPTY");
            }

            if (string.IsNullOrEmpty(address.Country))
            {
                logger.LogError($"{nameof(address.PostCode)} is empty");
                throw new UserValidationException("UPDATE_FARM_VAL_ADDRESS_POSTCODE_EMPTY");
            }

            if (string.IsNullOrEmpty(address.Country))
            {
                logger.LogError($"{nameof(address.Street)} is empty");
                throw new UserValidationException("UPDATE_FARM_VAL_ADDRESS_STREET_EMPTY");
            }
        }
    }
}
