﻿using System;
using FarmMachine.CommandHandlers.Exceptions;
using FarmMachine.CommandHandlers.Handlers.Base;
using FarmMachine.Commands.Farm;
using FarmMachine.Storage.Repositories;
using Microsoft.Extensions.Logging;
using FarmMachine.CommandHandlers.Factories;
using FarmMachine.Models.Entities;

namespace FarmMachine.CommandHandlers.Handlers.Farm
{
    public class DeleteFarmCommandHandler: ICommandHandler<DeleteFarmCommand>
    {
        private readonly IRepository<Models.Entities.Farm> farmRepository;
        private readonly IRepository<Models.Entities.DefaultUserInfo> defaultUserInfoRepository;
        private readonly ILogger<DeleteFarmCommandHandler> logger;
        private readonly IActionNotificationFactory actionNotificationFactory;
        private readonly IRepository<ActionNotification> actionNotificationRepository;

        public DeleteFarmCommandHandler(IRepository<Models.Entities.Farm> farmRepository,
            IRepository<Models.Entities.DefaultUserInfo> defaultUserInfoRepository,
            ILogger<DeleteFarmCommandHandler> logger,
            IRepository<ActionNotification> actionNotificationRepository,
            IActionNotificationFactory actionNotificationFactory)
        {
            this.farmRepository = farmRepository;
            this.defaultUserInfoRepository = defaultUserInfoRepository;
            this.logger = logger;
            this.actionNotificationFactory = actionNotificationFactory;
            this.actionNotificationRepository = actionNotificationRepository;
        }

        public void Execute(DeleteFarmCommand command)
        {
            var entity = farmRepository.Get(command.FarmId);

            if (entity == null)
            {
                logger.LogError($"farm [id:{command.FarmId}] does not exist");
                throw new UserValidationException("FARM_NOT_EXIST");
            }

            if (entity.CreatorUserId != command.UserId)
            {
                logger.LogError($"user [id:{command.UserId}] is not allowed to delete farm [id:{command.FarmId}");
                return;
            }

            entity.Deleted = true;
            entity.ModificationTime = DateTimeOffset.Now;

            var defaultUserInfo = defaultUserInfoRepository.Get(d => d.CreatorUserId == command.UserId && d.FarmId == command.FarmId);
            if (defaultUserInfo != null)
            {
                defaultUserInfo.FarmId = null;
            }

            var notification = actionNotificationFactory.Create(Models.Enums.ActionNotifyTypes.FarmDeleted, entity, command.UserId, entity.Id);
            actionNotificationRepository.Insert(notification);

            farmRepository.SaveChanges();
        }
        
    }
}
