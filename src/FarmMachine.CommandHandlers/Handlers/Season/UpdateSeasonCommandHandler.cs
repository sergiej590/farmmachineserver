﻿using System;
using FarmMachine.CommandHandlers.Exceptions;
using FarmMachine.CommandHandlers.Factories;
using FarmMachine.CommandHandlers.Handlers.Base;
using FarmMachine.Commands.Season;
using FarmMachine.Models.Entities;
using FarmMachine.Storage.Repositories;
using Microsoft.Extensions.Logging;
using System.Linq;
using FarmMachine.Storage.Extensions;

namespace FarmMachine.CommandHandlers.Handlers.Season
{
    public class UpdateSeasonCommandHandler:ICommandHandler<UpdateSeasonCommand>
    {
        private readonly IRepository<Models.Entities.Season> seasonRepository;
        private readonly IRepository<SeasonFarmManyRelation> seasonFarmRepository;
        private readonly ILogger<UpdateSeasonCommandHandler> logger;
        private readonly IActionNotificationFactory actionNotificationFactory;
        private readonly IRepository<ActionNotification> actionNotificationRepository;

        public UpdateSeasonCommandHandler(IRepository<Models.Entities.Season> seasonRepository,
            IRepository<SeasonFarmManyRelation> seasonFarmRepository,
            ILogger<UpdateSeasonCommandHandler> logger,
            IRepository<ActionNotification> actionNotificationRepository,
            IActionNotificationFactory actionNotificationFactory)
        {
            this.seasonRepository = seasonRepository;
            this.seasonFarmRepository = seasonFarmRepository;
            this.logger = logger;
            this.actionNotificationFactory = actionNotificationFactory;
            this.actionNotificationRepository = actionNotificationRepository;
        }
        public void Execute(UpdateSeasonCommand command)
        {
            InitModel(command);
            Validate(command);
            ExecuteAction(command);
        }

        private void InitModel(UpdateSeasonCommand command)
        {
            command.Model.Id = command.SeasonId;
            command.Model.ModificationTime = DateTimeOffset.Now;
            command.Model.Days = (command.Model.DateTo - command.Model.DateFrom).Days;
            command.Model.Farms = command.Model.Farms.Where(c => c.Checked);
            command.Model.FarmCount = command.Model.Farms.Count();
        }

        private void ExecuteAction(UpdateSeasonCommand command)
        {
            var entity = seasonRepository.GetSeason(s => s.Id == command.SeasonId);

            if (entity == null)
            {
                logger.LogError($"season [id:{command.SeasonId}] does not exist");
                throw new UserValidationException("SEASON_NOT_EXIST");
            }

            if (entity.CreatorUserId != command.UserId)
            {
                logger.LogError($"user [id:{command.UserId}] is not allowed to update season [id:{command.SeasonId}");
                throw new UserValidationException("USER_NOT_ALLOWED_TO_DELETE");
            }

            if (entity.Deleted)
            {
                logger.LogError($"season [id:{command.SeasonId}] is already deleted");
                throw new UserValidationException("SEASON_DELETED");
            }

            UpdateSeason(command, entity);

            var notification = actionNotificationFactory.Create(Models.Enums.ActionNotifyTypes.SeasonUpdated, null, command.UserId, entity.Id);
            actionNotificationRepository.Insert(notification);

            seasonRepository.SaveChanges();
        }

        private void UpdateSeason(UpdateSeasonCommand command, Models.Entities.Season entity)
        {
            entity.DateFrom = command.Model.DateFrom;
            entity.DateTo = command.Model.DateTo;
            entity.ModificationTime = command.Model.ModificationTime;
            entity.FarmCount = command.Model.FarmCount;
            entity.Days = command.Model.Days;

            foreach (var farmModel in command.Model.Farms)
            {
                var farm = entity.Farms.SingleOrDefault(c => c.FarmId == farmModel.Id);

                if (farm == null)
                {
                    farm = new SeasonFarmManyRelation()
                    {
                        Id = Guid.NewGuid(),
                        FarmId = farmModel.Id,
                        Season = entity
                    };
                    seasonFarmRepository.Insert(farm);
                }
            }

            var farmsToDelete =
                entity.Farms.Where(
                    c => command.Model.Farms.All(cm => cm.Id != c.FarmId)).ToList();
            foreach (var farmToDelete in farmsToDelete)
            {
                entity.Farms.Remove(farmToDelete);
            }
        }

        private void Validate(UpdateSeasonCommand command)
        {
            if (command.Model.DateFrom > command.Model.DateTo)
            {
                logger.LogError($"{nameof(command.Model.DateFrom)} ({command.Model.DateFrom}) is greater than {nameof(command.Model.DateTo)} ({command.Model.DateTo})");
                throw new UserValidationException("CREATE_SEASON_VAL_DATEFROM_GREATER_THAN_DATETO");
            }

            if (command.Model.Farms == null || !command.Model.Farms.Any())
            {
                logger.LogError($"{nameof(command.Model.Farms)} is empty");
                throw new UserValidationException("CREATE_SEASON_VAL_FARMS_EMPTY");
            }
        }
    }
}
