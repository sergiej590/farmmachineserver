﻿using System;
using FarmMachine.CommandHandlers.Exceptions;
using FarmMachine.CommandHandlers.Factories;
using FarmMachine.CommandHandlers.Handlers.Base;
using FarmMachine.Commands.Season;
using FarmMachine.Models.Entities;
using FarmMachine.Storage.Repositories;
using Microsoft.Extensions.Logging;

namespace FarmMachine.CommandHandlers.Handlers.Season
{
    public class DeleteSeasonCommandHandler : ICommandHandler<DeleteSeasonCommand>
    {
        private readonly IRepository<Models.Entities.Season> seasonRepository;
        private readonly IRepository<Models.Entities.DefaultUserInfo> defaultUserInfoRepository;
        private readonly ILogger<DeleteSeasonCommandHandler> logger;
        private readonly IActionNotificationFactory actionNotificationFactory;
        private readonly IRepository<ActionNotification> actionNotificationRepository;

        public DeleteSeasonCommandHandler(IRepository<Models.Entities.Season> seasonRepository,
            IRepository<Models.Entities.DefaultUserInfo> defaultUserInfoRepository,
            ILogger<DeleteSeasonCommandHandler> logger,
            IRepository<ActionNotification> actionNotificationRepository,
            IActionNotificationFactory actionNotificationFactory)
        {
            this.seasonRepository = seasonRepository;
            this.defaultUserInfoRepository = defaultUserInfoRepository;
            this.logger = logger;
            this.actionNotificationFactory = actionNotificationFactory;
            this.actionNotificationRepository = actionNotificationRepository;
        }

        public void Execute(DeleteSeasonCommand command)
        {
            var entity = seasonRepository.Get(command.SeasonId);

            if (entity == null)
            {
                logger.LogError($"season [id:{command.SeasonId}] does not exist");
                throw new UserValidationException("SEASON_NOT_EXIST");
            }

            if (entity.CreatorUserId != command.UserId)
            {
                logger.LogError($"user [id:{command.UserId}] is not allowed to delete season [id:{command.SeasonId}");
                return;
            }

            entity.Deleted = true;
            entity.ModificationTime = DateTimeOffset.Now;

            var defaultUserInfo = defaultUserInfoRepository.Get(d => d.CreatorUserId == command.UserId && d.SeasonId == command.SeasonId);
            if (defaultUserInfo != null)
            {
                defaultUserInfo.SeasonId = null;
            }

            var notification = actionNotificationFactory.Create(Models.Enums.ActionNotifyTypes.SeasonDeleted, null, command.UserId, entity.Id);
            actionNotificationRepository.Insert(notification);

            seasonRepository.SaveChanges();
        }
    }
}
