﻿using System;
using FarmMachine.CommandHandlers.Exceptions;
using FarmMachine.CommandHandlers.Handlers.Base;
using FarmMachine.Commands.Season;
using FarmMachine.Storage.Repositories;
using Microsoft.Extensions.Logging;
using System.Linq;
using FarmMachine.CommandHandlers.Factories;
using FarmMachine.Models.Entities;
using FarmMachine.Models.Enums;

namespace FarmMachine.CommandHandlers.Handlers.Season
{
    public class CreateSeasonCommandHandler : ICommandHandler<CreateSeasonCommand>
    {
        private readonly IRepository<Models.Entities.Season> seasonRepository;
        private readonly IRepository<SeasonFarmManyRelation> seasonFarmRepository;
        private readonly IRepository<Models.Entities.DefaultUserInfo> defaultUserInfoRepository;
        private readonly ILogger<CreateSeasonCommandHandler> logger;
        private readonly IRepository<ActionNotification> actionNotificationRepository;
        private readonly IActionNotificationFactory actionNotificationFactory;

        public CreateSeasonCommandHandler(IRepository<Models.Entities.Season> seasonRepository,
            IRepository<SeasonFarmManyRelation> seasonFarmRepository,
            IRepository<Models.Entities.DefaultUserInfo> defaultUserInfoRepository,
            ILogger<CreateSeasonCommandHandler> logger,
            IRepository<ActionNotification> actionNotificationRepository,
            IActionNotificationFactory actionNotificationFactory)
        {
            this.seasonRepository = seasonRepository;
            this.seasonFarmRepository = seasonFarmRepository;
            this.defaultUserInfoRepository = defaultUserInfoRepository;
            this.logger = logger;
            this.actionNotificationRepository = actionNotificationRepository;
            this.actionNotificationFactory = actionNotificationFactory;
        }

        public void Execute(CreateSeasonCommand command)
        {
            InitModel(command);
            Validate(command);
            ExecuteAction(command);
        }

        private void InitModel(CreateSeasonCommand command)
        {
            command.Model.CreationTime = DateTimeOffset.Now;
            command.Model.ModificationTime = DateTimeOffset.Now;
            command.Model.Days = (command.Model.DateTo - command.Model.DateFrom).Days;
            command.Model.Farms = command.Model.Farms.Where(c => c.Checked);
            command.Model.FarmCount = command.Model.Farms.Count();
        }

        private void ExecuteAction(CreateSeasonCommand command)
        {
            var season = CreateSeason(command);

            seasonRepository.Insert(season);

            var notification = actionNotificationFactory.Create(ActionNotifyTypes.SeasonCreated, null, 
                command.UserId, season.Id);
            actionNotificationRepository.Insert(notification);

            seasonRepository.SaveChanges();
        }

        private Models.Entities.Season CreateSeason(CreateSeasonCommand command)
        {
            var season = new Models.Entities.Season()
            {
                CreationTime = command.Model.CreationTime,
                ModificationTime = command.Model.ModificationTime,
                CreatorUserId = command.UserId,
                DateFrom = command.Model.DateFrom,
                DateTo = command.Model.DateTo,
                FarmCount = command.Model.FarmCount,
                Days = command.Model.Days
            };

            foreach (var farmModel in command.Model.Farms)
            {
                var farm = new SeasonFarmManyRelation
                {
                    Id = Guid.NewGuid(),
                    Season = season,
                    FarmId = farmModel.Id
                };
                seasonFarmRepository.Insert(farm);
            }

            var defaultUserInfo = defaultUserInfoRepository.Get(d => d.CreatorUserId == command.UserId && d.SeasonId == null);
            if (defaultUserInfo != null)
            {
                defaultUserInfo.Season = season;
            }

            return season;
        }

        private void Validate(CreateSeasonCommand command)
        {
            if (command.Model.DateFrom > command.Model.DateTo)
            {
                logger.LogError($"{nameof(command.Model.DateFrom)} ({command.Model.DateFrom}) is greater than {nameof(command.Model.DateTo)} ({command.Model.DateTo})");
                throw new UserValidationException("CREATE_SEASON_VAL_DATEFROM_GREATER_THAN_DATETO");
            }

            if (command.Model.Farms == null || !command.Model.Farms.Any())
            {
                logger.LogError($"{nameof(command.Model.Farms)} is empty");
                throw new UserValidationException("CREATE_SEASON_VAL_FARMS_EMPTY");
            }
        }
    }
}
