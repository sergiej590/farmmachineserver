﻿using System;
using FarmMachine.Models.Entities;
using FarmMachine.Models.Enums;

namespace FarmMachine.CommandHandlers.Factories
{
    public interface IActionNotificationFactory
    {
        ActionNotification Create(ActionNotifyTypes type, Farm farm, string userId, Guid objectId);
    }
}
