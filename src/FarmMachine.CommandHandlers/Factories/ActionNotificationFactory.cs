﻿using System;
using FarmMachine.Models.Entities;
using FarmMachine.Models.Enums;

namespace FarmMachine.CommandHandlers.Factories
{
    public class ActionNotificationFactory : IActionNotificationFactory
    {
        public ActionNotification Create(ActionNotifyTypes type, Farm farm, string userId, Guid objectId)
        {
            string title = type.ToString();
            string content = type.ToString();

            return new ActionNotification
            {
                CreationTime = DateTimeOffset.Now,
                CreatorUserId = userId,
                Farm = farm,
                ObjectId = objectId,
                Type = type,
                Title = title,
                Content = content
            };
        }
    }
}
