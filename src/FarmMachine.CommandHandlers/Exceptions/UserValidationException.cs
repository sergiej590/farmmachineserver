﻿namespace FarmMachine.CommandHandlers.Exceptions
{
    public class UserValidationException:UserException
    {
        public UserValidationException(string code) : base(code)
        {
        }
    }
}
