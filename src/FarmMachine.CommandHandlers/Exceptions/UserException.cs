﻿using System;

namespace FarmMachine.CommandHandlers.Exceptions
{
    public class UserException:Exception
    {
        public string Code { get; set; }

        public UserException(string code):base($"exception validation code [{code}]")
        {
            Code = code;
        }
    }
}
