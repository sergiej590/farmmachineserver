﻿using System;
using FarmMachine.Models.Entities;
using System.Linq;
using System.Linq.Expressions;
using FarmMachine.Storage.Repositories;
using Microsoft.EntityFrameworkCore;

namespace FarmMachine.Storage.Extensions
{
    public static class FarmRepositoryExtension
    {
        public static Farm GetFarm(this IRepository<Farm> repository, Expression<Func<Farm, bool>> expression )
        {
            return repository
                .GetQuerylable()
                .Include(f => f.Addresses)
                .Include(f => f.FarmCategories)
                .SingleOrDefault(expression);
        }
    }
}
