﻿using System;
using System.Linq;
using System.Linq.Expressions;
using FarmMachine.Models.Entities;
using FarmMachine.Storage.Repositories;
using Microsoft.EntityFrameworkCore;

namespace FarmMachine.Storage.Extensions
{
    public static class SeasonRepositoryExtension
    {
            public static Season GetSeason(this IRepository<Season> repository, Expression<Func<Season, bool>> expression)
            {
                return repository
                    .GetQuerylable()
                    .Include(f => f.Farms)
                    .SingleOrDefault(expression);
            }
    }
}
