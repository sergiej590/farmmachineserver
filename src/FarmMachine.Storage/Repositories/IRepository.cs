﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace FarmMachine.Storage.Repositories
{
    public interface IRepository<T>
    {
        IQueryable<T> GetQuerylable();
        void SaveChanges();
        void Insert(T entity);
        T Get(Guid id);
        T Get(Expression<Func<T, bool>> expression);
        IEnumerable<T> List(Expression<Func<T, bool>> expression = null);
        bool Any(Expression<Func<T, bool>> expression = null);
        void Delete(IEnumerable<T> entities);
        void Delete(T entity);
        IEnumerable<T> PaginatedList(Expression<Func<T, bool>> expression, Expression<Func<T, DateTimeOffset>> orderExpression, int page, int pageSize);
        int Count(Expression<Func<T, bool>> expression);
    }
}
