﻿using Microsoft.EntityFrameworkCore;
using FarmMachine.Models.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace FarmMachine.Storage.Repositories
{
    public class Repository<T> : IRepository<T> where T: class, IEntity
    {
        public DbContext Context { get; }

        public Repository(DbContext context)
        {
            Context = context;
        }

        public IQueryable<T> GetQuerylable()
        {
            return Context.Set<T>();
        }

        public void SaveChanges()
        {
            Context.SaveChanges();
        }

        public T Get(Guid id)
        {
            return Context.Set<T>()
                .SingleOrDefault(e => e.Id == id);
        }

        public T Get(Expression<Func<T, bool>> expression)
        {
            return Context.Set<T>()
                .SingleOrDefault(expression);
        }

        public IEnumerable<T> List(Expression<Func<T, bool>> expression = null)
        {
            if (expression != null)
            {
                return Context.Set<T>()
                    .Where(expression)
                    .ToList();
            }
            
            return Context.Set<T>()
                .ToList();
        }

        public bool Any(Expression<Func<T, bool>> expression = null)
        {
            if (expression != null)
            {
                return Context.Set<T>()
                    .Where(expression)
                    .Any();
            }

            return Context.Set<T>()
                .Any();
        }

        public void Insert(T entity)
        {
            Context.Set<T>().Add(entity);
        }

        public void Delete(IEnumerable<T> entities)
        {
            Context.Set<T>().RemoveRange(entities);
        }

        public void Delete(T entity)
        {
            Context.Set<T>().Remove(entity);
        }

        public IEnumerable<T> PaginatedList(Expression<Func<T, bool>> expression, Expression<Func<T, DateTimeOffset>> orderExpression, int page, int pageSize)
        {
            if (expression != null)
            {
                return Context.Set<T>()
                    .Where(expression)
                    .OrderByDescending(orderExpression)
                    .Skip((page-1)*pageSize)
                    .Take(pageSize)
                    .ToList();
            }

            return Context.Set<T>()
                .OrderByDescending(orderExpression)
                .Skip((page - 1) * pageSize)
                .Take(pageSize)
                .ToList();
        }

        public int Count(Expression<Func<T, bool>> expression)
        {
            if (expression != null)
            {
                return Context.Set<T>()
                    .Where(expression)
                    .Count();
            }

            return Context.Set<T>()
                .Count();
        }
    }
}
