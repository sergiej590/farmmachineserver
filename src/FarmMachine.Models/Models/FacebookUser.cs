﻿namespace FarmMachine.Models.Models
{
    public class FacebookUser
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public string First_Name { get; set; }
        public string Last_Name { get; set; }
    }
}
