﻿namespace FarmMachine.Models.Enums
{
    public enum ActionNotifyTypes
    {
        FarmCreated,
        FarmUpdated,
        FarmDeleted,
        SeasonCreated,
        SeasonDeleted,
        SeasonUpdated,
        DefaultUserInfoUpdated
    }
}
