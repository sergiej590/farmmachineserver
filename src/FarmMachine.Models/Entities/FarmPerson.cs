﻿using FarmMachine.Models.Base;

namespace FarmMachine.Models.Entities
{
    public class FarmPerson : UserEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Role { get; set; }
        public string PhoneNo { get; set; }
        public string Email { get; set; }
    }
}
