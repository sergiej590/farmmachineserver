﻿using System;
using FarmMachine.Models.Base;

namespace FarmMachine.Models.Entities
{
    public class SeasonFarmManyRelation:Entity
    {
        public Guid SeasonId { get; set; }
        public Season Season { get; set; }
        public Guid FarmId { get; set; }
        public Farm Farm { get; set; }
    }
}
