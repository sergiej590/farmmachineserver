﻿using System;
using System.Collections.Generic;
using FarmMachine.Models.Base;

namespace FarmMachine.Models.Entities
{
    public class Season :UserEntity
    {
        public DateTimeOffset CreationTime { get; set; }
        public DateTimeOffset ModificationTime { get; set; }
        public DateTimeOffset DateFrom { get; set; }
        public DateTimeOffset DateTo { get; set; }

        public int FarmCount { get; set; }
        public int Days { get; set; }
        public bool Deleted { get; set; }
        public ICollection<SeasonFarmManyRelation> Farms { get; set; }
    }
}
