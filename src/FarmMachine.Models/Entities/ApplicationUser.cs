﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace FarmMachine.Models.Entities
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser { }
}
