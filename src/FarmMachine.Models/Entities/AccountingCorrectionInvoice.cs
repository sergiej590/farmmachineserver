﻿using System;
using System.Collections.Generic;
using FarmMachine.Models.Base;

namespace FarmMachine.Models.Entities
{
    public class AccountingCorrectionInvoice:UserEntity
    {
        public DateTimeOffset CreationTime { get; set; }
        public DateTimeOffset ModificationTime { get; set; }

        public Guid? FarmId { get; set; }
        public Farm Farm { get; set; }

        public Guid InvoiceId { get; set; }
        public AccountingInvoice Invoice { get; set; }

        public DateTimeOffset IssueDate { get; set; }
        public DateTimeOffset SaleDate { get; set; }
        public DateTimeOffset PaymentDate { get; set; }

        public decimal GrossSum { get; set; }
        public decimal NetSum { get; set; }

        public string Comment { get; set; }

        public ICollection<AccountingInvoiceItem> Items { get; set; }
    }
}
