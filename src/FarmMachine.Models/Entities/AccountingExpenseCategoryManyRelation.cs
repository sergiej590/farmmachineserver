﻿using System;
using FarmMachine.Models.Base;

namespace FarmMachine.Models.Entities
{
    public class AccountingExpenseCategoryManyRelation:Entity
    {
        public Guid ExpenseId { get; set; }
        public AccountingExpense Expense { get; set; }

        public Guid ExpenseCategoryId { get; set; }
        public AccountingExpenseCategory ExpenseCategory { get; set; }
    }
}
