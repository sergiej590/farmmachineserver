﻿using System;
using FarmMachine.Models.Base;
using System.Collections.Generic;

namespace FarmMachine.Models.Entities
{
    public class Farm : UserEntity
    {
        public string Name { get; set; }
        public string FarmIdentifier { get; set; }
        public bool Active { get; set; }
        public DateTimeOffset CreationTime { get; set; }
        public DateTimeOffset ModificationTime { get; set; }
        public ICollection<FarmCategoryManyRelation> FarmCategories { get; set; }
        public ICollection<FarmAddress> Addresses { get; set; }
        public FarmPerson ContactPerson { get; set; }
        public bool Deleted { get; set; }
    }
}
