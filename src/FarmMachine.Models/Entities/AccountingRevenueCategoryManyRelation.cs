﻿using System;
using FarmMachine.Models.Base;

namespace FarmMachine.Models.Entities
{
    public class AccountingRevenueCategoryManyRelation:Entity
    {
        public Guid RevenueId { get; set; }
        public AccountingRevenue Revenue { get; set; }

        public Guid RevenueCategoryId { get; set; }
        public AccountingRevenueCategory RevenueCategory { get; set; }
    }
}
