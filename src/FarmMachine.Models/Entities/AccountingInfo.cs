﻿using System;
using FarmMachine.Models.Base;

namespace FarmMachine.Models.Entities
{
    public class AccountingInfo: UserEntity
    {
        public DateTimeOffset CreationTime { get; set; }
        public DateTimeOffset ModificationTime { get; set; }

        public string CompanyName { get; set; }
        public string CompanyAddressCity { get; set; }
        public string CompanyAddressPostCode { get; set; }
        public string CompanyAddressStreet { get; set; }
        public string CompanyAddressHomeNo { get; set; }

        public string CompanyAccountNo { get; set; }

        public bool VatPayer { get; set; }
        public string VatRegistrationNumber { get; set; }

        public Guid? FarmId { get; set; }
        public Farm Farm { get; set; }
        public bool Default { get; set; }
    }
}
