﻿using System;
using FarmMachine.Models.Base;

namespace FarmMachine.Models.Entities
{
    public class DefaultUserInfo : UserEntity
    {
        public Guid? FarmId { get; set; }
        public Farm Farm { get; set; }
        public Guid? SeasonId { get; set; }
        public Season Season { get; set; }
    }
}
