﻿using FarmMachine.Models.Base;

namespace FarmMachine.Models.Entities
{
    public class AccountingRevenueCategory:Entity
    {
        public string Name { get; set; }
    }
}
