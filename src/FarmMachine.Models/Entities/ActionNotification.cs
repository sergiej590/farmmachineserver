﻿using System;
using FarmMachine.Models.Base;
using FarmMachine.Models.Enums;

namespace FarmMachine.Models.Entities
{
    public class ActionNotification:UserEntity
    {
        public DateTimeOffset CreationTime { get; set; }

        public string Title { get; set; }
        public string Content { get; set; }
        public Guid? FarmId { get; set; }
        public Farm Farm { get; set; }
        public Guid? ObjectId { get; set; }
        public ActionNotifyTypes Type { get; set; }
    }
}
