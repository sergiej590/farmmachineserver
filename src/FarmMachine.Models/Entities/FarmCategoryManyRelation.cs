﻿using System;
using FarmMachine.Models.Base;

namespace FarmMachine.Models.Entities
{
    public class FarmCategoryManyRelation:Entity
    {
        public Guid FarmId { get; set; }
        public Farm Farm { get; set; }

        public Guid FarmCategoryId { get; set; }
        public FarmCategory FarmCategory { get; set; }
    }
}
