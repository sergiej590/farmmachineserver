﻿using System;
using System.Collections.Generic;
using FarmMachine.Models.Base;

namespace FarmMachine.Models.Entities
{
    public class AccountingRevenue:UserEntity
    {
        public DateTimeOffset CreationTime { get; set; }
        public DateTimeOffset ModificationTime { get; set; }

        public Guid? FarmId { get; set; }
        public Farm Farm { get; set; }

        public decimal NetValue { get; set; }
        public decimal GrossValue { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }

        public DateTimeOffset Date { get; set; }

        public Guid? InvoiceId { get; set; }
        public AccountingInvoice Invoice { get; set; }

        public bool Planned { get; set; }

        public Guid? PlannedRevenueId { get; set; }
        public AccountingPlannedRevenue PlannedRevenue { get; set; }

        public ICollection<AccountingRevenueCategoryManyRelation> RevenueCategories { get; set; }

        public Guid SeasonId { get; set; }
        public Season Season { get; set; }
    }
}
