﻿using System;
using System.Collections.Generic;
using FarmMachine.Models.Base;

namespace FarmMachine.Models.Entities
{
    public class AccountingInvoice:UserEntity
    {
        public DateTimeOffset CreationTime { get; set; }
        public DateTimeOffset ModificationTime { get; set; }

        public Guid? FarmId { get; set; }
        public Farm Farm { get; set; }

        public Guid? AccountingInfoId { get; set; }
        public AccountingInfo AccountingInfo { get; set; }

        public string FromCompanyName { get; set; }
        public string FromCompanyAddressCity { get; set; }
        public string FromCompanyAddressPostCode { get; set; }
        public string FromCompanyAddressStreet { get; set; }
        public string FromCompanyAddressHomeNo { get; set; }
        public string FromCompanyAccountNo { get; set; }
        public string FromCompanyVatRegistrationNumber { get; set; }

        public string ToCompanyName { get; set; }
        public string ToCompanyAddressCity { get; set; }
        public string ToCompanyAddressPostCode { get; set; }
        public string ToCompanyAddressStreet { get; set; }
        public string ToCompanyAddressHomeNo { get; set; }
        public string ToCompanyVatRegistrationNumber { get; set; }

        public DateTimeOffset IssueDate { get; set; }
        public DateTimeOffset SaleDate { get; set; }
        public DateTimeOffset PaymentDate { get; set; }
        public bool Paid { get; set; }

        public decimal GrossSum { get; set; }
        public decimal NetSum { get; set; }

        public ICollection<AccountingInvoiceItem> Items { get; set; }

        public Guid SeasonId { get; set; }
        public Season Season { get; set; }
    }
}
