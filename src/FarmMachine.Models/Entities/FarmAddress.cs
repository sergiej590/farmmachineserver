﻿using FarmMachine.Models.Base;

namespace FarmMachine.Models.Entities
{
    public class FarmAddress:Entity
    {
        public string Country { get; set; }
        public string PostCode { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        public string HomeNo { get; set; }
        public bool Main { get; set; }
    }
}
