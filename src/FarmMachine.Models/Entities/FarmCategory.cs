﻿using System.Collections.Generic;
using FarmMachine.Models.Base;

namespace FarmMachine.Models.Entities
{
    public class FarmCategory:Entity
    {
        public string Name { get; set; }

        public ICollection<FarmCategoryManyRelation> FarmCategories { get; set; }
    }
}
