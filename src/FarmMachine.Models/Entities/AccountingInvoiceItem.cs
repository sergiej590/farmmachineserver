﻿using FarmMachine.Models.Base;

namespace FarmMachine.Models.Entities
{
    public class AccountingInvoiceItem: Entity
    {
        public string Name { get; set; }
        public string Pkuiw { get; set; }
        public int Amount { get; set; }
        public decimal UnitValue { get; set; }
        public int VatValue { get; set; }
        public decimal NetValue { get; set; }
        public decimal GrossValue { get; set; }
    }
}
