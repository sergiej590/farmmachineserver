﻿using FarmMachine.Models.Base;

namespace FarmMachine.Models.Entities
{
    public class AccountingExpenseCategory:Entity
    {
        public string Name { get; set; }
    }
}
