﻿using System;
using System.Collections.Generic;
using FarmMachine.Models.Base;

namespace FarmMachine.Models.Entities
{
    public class AccountingPlannedExpense:UserEntity
    {
        public DateTimeOffset CreationTime { get; set; }
        public DateTimeOffset ModificationTime { get; set; }

        public Guid? FarmId { get; set; }
        public Farm Farm { get; set; }

        public decimal NetValue { get; set; }
        public decimal GrossValue { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }

        public DateTimeOffset Date { get; set; }

        public DateTimeOffset DateFrom { get; set; }
        public DateTimeOffset DateTo { get; set; }


        public ICollection<AccountingExpenseCategoryManyRelation> ExpenseCategories { get; set; }
    }
}
