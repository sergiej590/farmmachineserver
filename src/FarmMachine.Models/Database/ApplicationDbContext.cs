﻿using FarmMachine.Models.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using OpenIddict;

namespace FarmMachine.Models.Database
{
    public class ApplicationDbContext : OpenIddictDbContext<ApplicationUser, IdentityRole>
    {
        public ApplicationDbContext(DbContextOptions options)
            : base(options) {
        }

        public DbSet<Farm> Farms { get; set; }
        public DbSet<FarmAddress> FarmAddresses { get; set; }
        public DbSet<FarmCategory> FarmCategories { get; set; }
        public DbSet<FarmPerson> FarmPeople { get; set; }
        
        public DbSet<Season> Seasons { get; set; }

        public DbSet<DefaultUserInfo> DefaultUserInfos { get; set; }

        public DbSet<ActionNotification> ActionNotifications { get; set; }
        
        public DbSet<AccountingInfo> AccountingInfos { get; set; }
        public DbSet<AccountingInvoice> AccountingInvoices { get; set; }
        public DbSet<AccountingCorrectionInvoice> AccountingCorrectionInvoices { get; set; }
        public DbSet<AccountingInvoiceItem> AccountingInvoiceItems { get; set; }
        public DbSet<AccountingExpense> AccountingExpenses { get; set; }
        public DbSet<AccountingExpenseCategory> AccountingExpenseCategories { get; set; }
        public DbSet<AccountingRevenue> AccountingRevenues { get; set; }
        public DbSet<AccountingRevenueCategory> AccountingRevenueCategories { get; set; }



        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
        }
    }
}
