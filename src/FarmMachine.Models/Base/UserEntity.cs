﻿using FarmMachine.Models.Entities;

namespace FarmMachine.Models.Base
{
    public class UserEntity : Entity
    {
        public string CreatorUserId { get; set; }
        public ApplicationUser CreatorUser { get; set; }
    }
}
