﻿using System;

namespace FarmMachine.Models.Base
{
    public interface IEntity
    {
        Guid Id { get; set; }
    }
}
