﻿using Autofac;
using AutoMapper;
using FarmMachine.CommandHandlers.Factories;
using FarmMachine.CommandHandlers.Handlers.Base;
using FarmMachine.CommandHandlers.Handlers.DefaultUserInfo;
using FarmMachine.CommandHandlers.Handlers.Farm;
using FarmMachine.CommandHandlers.Handlers.Season;
using FarmMachine.CommandHandlers.Handlers.User;
using FarmMachine.Commands.DefaultUserInfo;
using FarmMachine.Commands.Farm;
using FarmMachine.Commands.Season;
using FarmMachine.Commands.User;
using FarmMachine.Dto.Base;
using FarmMachine.Dto.DefaultUserInfo;
using FarmMachine.Dto.Farm;
using FarmMachine.Dto.Season;
using FarmMachine.Messaging.Busses;
using FarmMachine.Messaging.Dispatchers;
using FarmMachine.Messaging.Factories;
using FarmMachine.Models.Database;
using FarmMachine.Queries.DefaultUserInfo;
using FarmMachine.Queries.Farm;
using FarmMachine.Queries.Season;
using FarmMachine.QueryHandlers.Handlers.Base;
using FarmMachine.QueryHandlers.Handlers.DefaultUserInfo;
using FarmMachine.QueryHandlers.Handlers.Farm;
using FarmMachine.QueryHandlers.Handlers.Season;
using FarmMachine.Services.Implementation;
using FarmMachine.Services.Interfaces;
using FarmMachine.Storage.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace FarmMachine.Api.Configurations
{
    public class AutofacConfiguration
    {
        public ContainerBuilder CreateContainerBuilder(IConfigurationRoot configuration)
        {
            var builder = new ContainerBuilder();

            builder.RegisterInstance(configuration).As<IConfigurationRoot>();

            var autoMapperConfiguration = new AutoMapperConfiguration();
            var mapper = autoMapperConfiguration.CreateMapper();
            builder.RegisterInstance(mapper).As<IMapper>();

            builder.RegisterType<CommandHandlerFactory>().As<ICommandHandlerFactory>();
            builder.RegisterType<InMemoryCommandBus>().As<ICommandBus>();
            builder.RegisterType<QueryHandlerFactory>().As<IQueryHandlerFactory>();
            builder.RegisterType<QueryDispatcher>().As<IQueryDispatcher>();

            builder.RegisterType<ApplicationDbContext>().As<DbContext>();
            builder.RegisterGeneric(typeof(Repository<>)).As(typeof(IRepository<>));

            builder.RegisterType<RegisterExternalUserCommandHandler>().As<ICommandHandler<RegisterFacebookUserCommand>>();

            builder.RegisterType<CreateFarmCommandHandler>().As<ICommandHandler<CreateFarmCommand>>();
            builder.RegisterType<UpdateFarmCommandHandler>().As<ICommandHandler<UpdateFarmCommand>>();
            builder.RegisterType<DeleteFarmCommandHandler>().As<ICommandHandler<DeleteFarmCommand>>();
            builder.RegisterType<GetFarmQueryHandler>().As<IQueryHandler<GetFarmQuery, FarmDto>>();
            builder.RegisterType<GetFarmsQueryHandler>().As<IQueryHandler<GetFarmsQuery, ListDto<SimpleFarmDto>>>();
            builder.RegisterType<GetFarmCategoriesQueryHandler>().As<IQueryHandler<GetFarmCategoriesQuery, ListDto<FarmCategoryDto>>>();
            
            builder.RegisterType<CreateSeasonCommandHandler>().As<ICommandHandler<CreateSeasonCommand>>();
            builder.RegisterType<DeleteSeasonCommandHandler>().As<ICommandHandler<DeleteSeasonCommand>>();
            builder.RegisterType<UpdateSeasonCommandHandler>().As<ICommandHandler<UpdateSeasonCommand>>();
            builder.RegisterType<GetSeasonsQueryHandler>().As<IQueryHandler<GetSeasonsQuery, PaginatedListDto<SimpleSeasonDto>>>();
            builder.RegisterType<GetSeasonQueryHandler>().As<IQueryHandler<GetSeasonQuery, SeasonDto>>();

            builder.RegisterType<UpdateDefaultUserInfoCommandHandler>().As<ICommandHandler<UpdateDefaultUserInfoCommand>>();
            builder.RegisterType<GetDefaultUserInfoQueryHandler>().As<IQueryHandler<GetDefaultUserInfoQuery, DefaultUserInfoDto>>();
            
            builder.RegisterType<ActionNotificationFactory>().As<IActionNotificationFactory>();

            builder.RegisterType<RestService>().As<IRestService>();

            return builder;
        }
    }
}
