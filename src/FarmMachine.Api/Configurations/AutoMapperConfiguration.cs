﻿using AutoMapper;
using FarmMachine.QueryHandlers.Mappings;

namespace FarmMachine.Api.Configurations
{
    public class AutoMapperConfiguration
    {
        public IMapper CreateMapper()
        {
            var configuration = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<FarmProfile>();
                cfg.AddProfile<SeasonProfile>();
                cfg.AddProfile<DefaultUserInfoProfile>();
            });

            return configuration.CreateMapper();
        }
    }
}
