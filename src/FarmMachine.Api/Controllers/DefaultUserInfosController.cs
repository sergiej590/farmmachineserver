﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using FarmMachine.Messaging.Dispatchers;
using FarmMachine.Messaging.Busses;
using FarmMachine.Commands.DefaultUserInfo;
using FarmMachine.CommandHandlers.Exceptions;
using System;
using FarmMachine.Dto.DefaultUserInfo;
using FarmMachine.Queries.DefaultUserInfo;

namespace FarmMachine.Api.Controllers
{

    [Authorize]
    [Route("api/[controller]")]
    public class DefaultUserInfosController : AuthorizeController
    {
        private readonly IQueryDispatcher queryDispatcher;
        private readonly ICommandBus commandBus;
        private readonly ILogger<FarmsController> logger;

        public DefaultUserInfosController(IQueryDispatcher queryDispatcher,
            ICommandBus commandBus,
            ILogger<FarmsController> logger) : base(logger)
        {
            this.queryDispatcher = queryDispatcher;
            this.commandBus = commandBus;
            this.logger = logger;
        }

        [HttpGet]
        public IActionResult GetDefaultUserInfo()
        {
            try
            {
                var query = new GetDefaultUserInfoQuery(UserId);
                var result = queryDispatcher.Dispatch<GetDefaultUserInfoQuery, DefaultUserInfoDto>(query);

                return Ok(result);
            }
            catch (Exception ex)
            {
                logger.LogError($"error during getting default user info for user [name:{User.Identity.Name}]", ex);
                return BadRequest();
            }
        }

        [HttpPut]
        public IActionResult UpdateDefaultUserInfo([FromBody]DefaultUserInfoDto defaultUserInfo)
        {
            try
            {
                var command = new UpdateDefaultUserInfoCommand(UserId, defaultUserInfo.FarmId, defaultUserInfo.SeasonId);
                commandBus.Send(command);

                return Ok(defaultUserInfo);
            }
            catch (UserException ex)
            {
                return BadRequest(ex.Code);
            }
            catch (Exception ex)
            {
                logger.LogError("error during updating default user info", ex);
                return BadRequest();
            }
        }
    }
}
