﻿using System.Linq;
using System.Threading.Tasks;
using AspNet.Security.OpenIdConnect.Extensions;
using AspNet.Security.OpenIdConnect.Server;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http.Authentication;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using OpenIddict;
using FarmMachine.Models.Entities;
using FarmMachine.Models.Models;
using FarmMachine.Services.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace FarmMachine.Api.Controllers
{
    public class AuthorizationController : Controller
    {
        private readonly UserManager<ApplicationUser> userManager;
        private readonly SignInManager<ApplicationUser> signInManager;
        private readonly IConfigurationRoot configuration;
        private readonly IRestService restService;
        private readonly ILogger<AuthorizationController> logger;

        public AuthorizationController(UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            IConfigurationRoot configuration,
            IRestService restService,
            ILogger<AuthorizationController> logger)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.configuration = configuration;
            this.restService = restService;
            this.logger = logger;
        }
        
        [HttpPost("~/connect/token")]
        [Produces("application/json")]
        public async Task<IActionResult> Exchange(OpenIdConnectRequest request)
        {
            if (request.IsPasswordGrantType())
            {
                return await PasswordFlow(request);
            }
            else if (request.GrantType == "urn:ietf:params:oauth:grant-type:facebook_identity_token")
            {
                return await CustomFacebookFlow(request);
            }

            logger.LogError($"the specified grant type is not supported [grant type: {request.GrantType}]");

            return BadRequest(new OpenIdConnectResponse
            {
                Error = OpenIdConnectConstants.Errors.UnsupportedGrantType,
                ErrorDescription = "The specified grant type is not supported."
            });
        }

        private async Task<IActionResult> CustomFacebookFlow(OpenIdConnectRequest request)
        {
            if (string.IsNullOrEmpty(request.Assertion))
            {
                logger.LogError("the mandatory 'assertion' parameter was missing");

                return BadRequest(new OpenIdConnectResponse
                {
                    Error = OpenIdConnectConstants.Errors.InvalidRequest,
                    ErrorDescription = "The mandatory 'assertion' parameter was missing."
                });
            }

            var urlFacebook = $"{configuration["TokenAuthentication:ExternalProviders:Facebook:GetUserInfoUrl"]}{request.Assertion}";
            var facebookUser = await restService.Get<FacebookUser>(urlFacebook);
            
            if(facebookUser == null)
            {
                logger.LogError("the facebook user was not authenticated");

                return BadRequest(new OpenIdConnectResponse
                {
                    Error = OpenIdConnectConstants.Errors.InvalidRequest,
                    ErrorDescription = "The facebook user was not authenticated."
                });
            }

            var user = await userManager.FindByNameAsync(facebookUser.Email);
            if (user == null)
            {
                logger.LogError($"the email [{facebookUser.Email}] is invalid");

                return BadRequest(new OpenIdConnectResponse
                {
                    Error = OpenIdConnectConstants.Errors.InvalidGrant,
                    ErrorDescription = "The email is invalid."
                });
            }
            
            if (!await signInManager.CanSignInAsync(user))
            {
                logger.LogError($"the specified user [{user.Email}] is not allowed to sign in");

                return BadRequest(new OpenIdConnectResponse
                {
                    Error = OpenIdConnectConstants.Errors.InvalidGrant,
                    ErrorDescription = "The specified user is not allowed to sign in."
                });
            }

            if (userManager.SupportsUserTwoFactor && await userManager.GetTwoFactorEnabledAsync(user))
            {
                logger.LogError($"The specified user [{user.Email}] is not allowed to sign in");

                return BadRequest(new OpenIdConnectResponse
                {
                    Error = OpenIdConnectConstants.Errors.InvalidGrant,
                    ErrorDescription = "The specified user require two-factor authentication."
                });
            }
            
            if (userManager.SupportsUserLockout && await userManager.IsLockedOutAsync(user))
            {
                logger.LogError($"the user [{user.Email}] is not already locked out");

                return BadRequest(new OpenIdConnectResponse
                {
                    Error = OpenIdConnectConstants.Errors.InvalidGrant,
                    ErrorDescription = "The user is not already locked out."
                });
            }

            var userInfo = await userManager.GetLoginsAsync(user);
            if (userInfo == null || !userInfo.Any())
            {
                logger.LogError($"the user [{user.Email}] is not connected with external providers");
                
                return BadRequest(new OpenIdConnectResponse
                {
                    Error = OpenIdConnectConstants.Errors.InvalidGrant,
                    ErrorDescription = "The user is not connected with external providers."
                });
            }

            if (!userInfo.Any(ui => ui.LoginProvider == "Facebook" && ui.ProviderKey == facebookUser.Id))
            {
                logger.LogError($"the user [{user.Email}] is not connected with facebook");
                
                return BadRequest(new OpenIdConnectResponse
                {
                    Error = OpenIdConnectConstants.Errors.InvalidGrant,
                    ErrorDescription = "The user is not connected with facebook."
                });
            }
            
            var ticket = await CreateTicketAsync(request, user);

            return SignIn(ticket.Principal, ticket.Properties, ticket.AuthenticationScheme);
        }

        private async Task<IActionResult> PasswordFlow(OpenIdConnectRequest request)
        {
            var user = await userManager.FindByNameAsync(request.Username);
            if (user == null)
            {
                logger.LogError($"the username [{request.Username}] is invalid");
                
                return BadRequest(new OpenIdConnectResponse
                {
                    Error = OpenIdConnectConstants.Errors.InvalidGrant,
                    ErrorDescription = $"The username is invalid."
                });
            }
            
            if (!await signInManager.CanSignInAsync(user))
            {
                logger.LogError($"the specified user [{user.UserName}] is not allowed to sign in");

                return BadRequest(new OpenIdConnectResponse
                {
                    Error = OpenIdConnectConstants.Errors.InvalidGrant,
                    ErrorDescription = $"The specified user is not allowed to sign in."
                });
            }

            if (userManager.SupportsUserTwoFactor && await userManager.GetTwoFactorEnabledAsync(user))
            {
                logger.LogError($"the specified user [{user.UserName}] required two-factor authentication");
                
                return BadRequest(new OpenIdConnectResponse
                {
                    Error = OpenIdConnectConstants.Errors.InvalidGrant,
                    ErrorDescription = $"The specified user required two-factor authentication."
                });
            }
            
            if (userManager.SupportsUserLockout && await userManager.IsLockedOutAsync(user))
            {
                logger.LogError($"the username [{user.UserName}] is locked out");

                return BadRequest(new OpenIdConnectResponse
                {
                    Error = OpenIdConnectConstants.Errors.InvalidGrant,
                    ErrorDescription = "The username is locked out."
                });
            }
            
            if (!await userManager.CheckPasswordAsync(user, request.Password))
            {
                logger.LogError($"the username/password [{user.UserName}] couple is invalid");

                if (userManager.SupportsUserLockout)
                {
                    await userManager.AccessFailedAsync(user);
                }

                return BadRequest(new OpenIdConnectResponse
                {
                    Error = OpenIdConnectConstants.Errors.InvalidGrant,
                    ErrorDescription = "The username/password couple is invalid."
                });
            }

            if (userManager.SupportsUserLockout)
            {
                await userManager.ResetAccessFailedCountAsync(user);
            }

            var ticket = await CreateTicketAsync(request, user);

            return SignIn(ticket.Principal, ticket.Properties, ticket.AuthenticationScheme);
        }

        private async Task<AuthenticationTicket> CreateTicketAsync(OpenIdConnectRequest request, ApplicationUser user)
        {
            // Create a new ClaimsPrincipal containing the claims that
            // will be used to create an id_token, a token or a code.
            var principal = await signInManager.CreateUserPrincipalAsync(user);

            var identity = principal.Identities.FirstOrDefault();
            identity?.AddClaim("user-id", user.Id);

            // Note: by default, claims are NOT automatically included in the access and identity tokens.
            // To allow OpenIddict to serialize them, you must attach them a destination, that specifies
            // whether they should be included in access tokens, in identity tokens or in both.

            foreach (var claim in principal.Claims)
            {
                // In this sample, every claim is serialized in both the access and the identity tokens.
                // In a real world application, you'd probably want to exclude confidential claims
                // or apply a claims policy based on the scopes requested by the client application.
                claim.SetDestinations(OpenIdConnectConstants.Destinations.AccessToken,
                                      OpenIdConnectConstants.Destinations.IdentityToken);
            }

            // Create a new authentication ticket holding the user identity.
            var ticket = new AuthenticationTicket(
                principal, new AuthenticationProperties(),
                OpenIdConnectServerDefaults.AuthenticationScheme);

            // Set the list of scopes granted to the client application.
            // Note: the offline_access scope must be granted
            // to allow OpenIddict to return a refresh token.
            ticket.SetScopes(new[] {
                OpenIdConnectConstants.Scopes.OpenId,
                OpenIdConnectConstants.Scopes.Email,
                OpenIdConnectConstants.Scopes.Profile,
                OpenIdConnectConstants.Scopes.OfflineAccess,
                OpenIddictConstants.Scopes.Roles
            }.Intersect(request.GetScopes()));

            return ticket;
        }
    }
}
