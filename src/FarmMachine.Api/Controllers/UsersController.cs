﻿using Microsoft.AspNetCore.Mvc;
using FarmMachine.Messaging.Busses;
using FarmMachine.Dto;
using System;
using FarmMachine.CommandHandlers.Exceptions;
using FarmMachine.Commands.User;
using Microsoft.Extensions.Logging;

namespace FarmMachine.Api.Controllers
{
    [Route("api/[controller]")]
    public class UsersController : Controller
    {
        private readonly ICommandBus commandBus;
        private readonly ILogger<UsersController> logger;

        public UsersController(ICommandBus commandBus,
                               ILogger<UsersController> logger)
        {
            this.commandBus = commandBus;
            this.logger = logger;
        }
        
        [HttpPost]
        [Route("registerExternal")]
        public IActionResult RegisterExternalUser([FromBody]UserDto user)
        {
            try
            {
                var command = new RegisterFacebookUserCommand(Guid.NewGuid(), -1, user.AccessToken);
                commandBus.Send(command);

                return Ok();
            }
            catch (UserException ex)
            {
                return BadRequest(ex.Code);
            }
            catch (Exception ex)
            {
                logger.LogError("error during registering external user", ex);
                return BadRequest();
            }
        }
        
    }
}
