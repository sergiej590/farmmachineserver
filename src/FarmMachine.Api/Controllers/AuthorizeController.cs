﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace FarmMachine.Api.Controllers
{
    [Authorize]
    public class AuthorizeController : Controller
    {
        private readonly ILogger logger;

        public AuthorizeController(ILogger logger)
        {
            this.logger = logger;
        }
        public string UserId
        {
            get
            {
                var userId = User.Claims.SingleOrDefault(c => c.Type == "user-id");

                if (userId == null)
                {
                    logger.LogError("the user-id claim does not exist");
                    throw new Exception("the user-id claim does not exist");
                }

                return userId.Value;
            }
        }
    }
}
