﻿using System;
using FarmMachine.CommandHandlers.Exceptions;
using FarmMachine.Commands.Farm;
using FarmMachine.Dto.Base;
using FarmMachine.Dto.Farm;
using FarmMachine.Messaging.Busses;
using FarmMachine.Messaging.Dispatchers;
using FarmMachine.Queries.Farm;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace FarmMachine.Api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class FarmsController : AuthorizeController
    {
        private readonly IQueryDispatcher queryDispatcher;
        private readonly ICommandBus commandBus;
        private readonly ILogger<FarmsController> logger;

        public FarmsController(IQueryDispatcher queryDispatcher,
            ICommandBus commandBus,
            ILogger<FarmsController> logger) : base(logger)
        {
            this.queryDispatcher = queryDispatcher;
            this.commandBus = commandBus;
            this.logger = logger;
        }

        [HttpGet]
        public IActionResult GetFarms()
        {
            try
            {
                var query = new GetFarmsQuery(UserId);
                var result = queryDispatcher.Dispatch<GetFarmsQuery, ListDto<SimpleFarmDto>>(query);

                return Ok(result.Items);
            }
            catch (Exception ex)
            {
                logger.LogError($"error during getting farms for user [name:{User.Identity.Name}]", ex);
                return BadRequest();
            }
        }

        [HttpGet("{id:Guid}", Name ="GetFarm")]
        public IActionResult GetFarm(Guid id)
        {
            try
            {
                var query = new GetFarmQuery(id, UserId);
                var result = queryDispatcher.Dispatch<GetFarmQuery, FarmDto>(query);

                if (result == null)
                {
                    return NotFound();
                }

                return Ok(result);
            }
            catch (Exception ex)
            {
                logger.LogError($"error during getting farm [id:{id}]", ex);
                return BadRequest();
            }
        }

        [HttpGet("categories")]
        public IActionResult GetCategories() 
        {
            try
            {
                var query = new GetFarmCategoriesQuery();
                var result = queryDispatcher.Dispatch<GetFarmCategoriesQuery, ListDto<FarmCategoryDto>>(query);

                return Ok(result.Items);
            }
            catch (Exception ex)
            {
                logger.LogError("error during getting farm categories", ex);
                return BadRequest();
            }
        }

        [HttpPost]
        public IActionResult CreateFarm([FromBody]FarmDto farm)
        {
            try
            {
                var command = new CreateFarmCommand(farm, UserId);
                commandBus.Send(command);

                return Created("GetFarm", farm);
            }
            catch (UserException ex)
            {
                return BadRequest(ex.Code);
            }
            catch (Exception ex)
            {
                logger.LogError("error during creating farm", ex);
                return BadRequest();
            }
        }

        [HttpPut("{id:Guid}")]
        public IActionResult UpdateFarm(Guid id, [FromBody]FarmDto farm)
        {
            try
            {
                var command = new UpdateFarmCommand(id, farm, UserId);
                commandBus.Send(command);

                return Ok(farm);
            }
            catch (UserException ex)
            {
                return BadRequest(ex.Code);
            }
            catch (Exception ex)
            {
                logger.LogError("error during updating farm", ex);
                return BadRequest();
            }
        }

        [HttpDelete("{id:Guid}")]
        public IActionResult DeleteFarm(Guid id)
        {
            try
            {
                var command = new DeleteFarmCommand(id, UserId);
                commandBus.Send(command);

                return Ok();
            }
            catch (UserException ex)
            {
                return BadRequest(ex.Code);
            }
            catch (Exception ex)
            {
                logger.LogError("error during deleting farm", ex);
                return BadRequest();
            }
        }
    }
}
