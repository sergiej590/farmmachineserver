﻿using System;
using FarmMachine.CommandHandlers.Exceptions;
using FarmMachine.Commands.Season;
using FarmMachine.Dto.Base;
using FarmMachine.Dto.Season;
using FarmMachine.Messaging.Busses;
using FarmMachine.Messaging.Dispatchers;
using FarmMachine.Queries.Farm;
using FarmMachine.Queries.Season;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace FarmMachine.Api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class SeasonsController : AuthorizeController
    {
        private readonly IQueryDispatcher queryDispatcher;
        private readonly ICommandBus commandBus;
        private readonly ILogger<SeasonsController> logger;

        public SeasonsController(IQueryDispatcher queryDispatcher,
            ICommandBus commandBus,
            ILogger<SeasonsController> logger) : base(logger)
        {
            this.queryDispatcher = queryDispatcher;
            this.commandBus = commandBus;
            this.logger = logger;
        }

        [HttpGet]
        public IActionResult GetSeasons(DateTimeOffset? dateFrom, DateTimeOffset? dateTo, int page = 1, int pageSize =10)
        {
            try
            {
                var query = new GetSeasonsQuery(UserId, dateFrom, dateTo, page, pageSize);
                var result = queryDispatcher.Dispatch<GetSeasonsQuery, PaginatedListDto<SimpleSeasonDto>>(query);

                return Ok(result);
            }
            catch (Exception ex)
            {
                logger.LogError($"error during getting seasons for user [name:{User.Identity.Name}]", ex);
                return BadRequest();
            }
        }

        [HttpPost]
        public IActionResult CreateSeason([FromBody]SeasonDto season)
        {
            try
            {
                var command = new CreateSeasonCommand(season, UserId);
                commandBus.Send(command);

                return Created("GetSeason", season);
            }
            catch (UserException ex)
            {
                return BadRequest(ex.Code);
            }
            catch (Exception ex)
            {
                logger.LogError("error during creating season", ex);
                return BadRequest();
            }
        }

        [HttpGet("{id:Guid}", Name = "GetSeason")]
        public IActionResult GetSeason(Guid id)
        {
            try
            {
                var query = new GetSeasonQuery(UserId, id);
                var result = queryDispatcher.Dispatch<GetSeasonQuery, SeasonDto>(query);

                if (result == null)
                {
                    return NotFound();
                }

                return Ok(result);
            }
            catch (Exception ex)
            {
                logger.LogError($"error during getting season [id:{id}]", ex);
                return BadRequest();
            }
        }

        [HttpPut("{id:Guid}")]
        public IActionResult UpdateSeason(Guid id, [FromBody]SeasonDto farm)
        {
            try
            {
                var command = new UpdateSeasonCommand(id, farm, UserId);
                commandBus.Send(command);

                return Ok(farm);
            }
            catch (UserException ex)
            {
                return BadRequest(ex.Code);
            }
            catch (Exception ex)
            {
                logger.LogError("error during updating season", ex);
                return BadRequest();
            }
        }

        [HttpDelete("{id:Guid}")]
        public IActionResult DeleteSeason(Guid id)
        {
            try
            {
                var command = new DeleteSeasonCommand(id, UserId);
                commandBus.Send(command);

                return Ok();
            }
            catch (UserException ex)
            {
                return BadRequest(ex.Code);
            }
            catch (Exception ex)
            {
                logger.LogError("error during deleting season", ex);
                return BadRequest();
            }
        }
    }
}
