﻿using Autofac;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Security.Cryptography.X509Certificates;
using FarmMachine.Api.Configurations;
using FarmMachine.Models.Database;
using FarmMachine.Models.Entities;
using Autofac.Extensions.DependencyInjection;
using Serilog;

namespace FarmMachine.Api
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddMvc();
            services.AddCors();

            services.AddDbContext<ApplicationDbContext>(options =>
                  options.UseSqlServer(Configuration["Data:DefaultConnection:ConnectionString"],
                  b => b.MigrationsAssembly("FarmMachine.Api")));

            // Register the Identity services.
            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            var accessTokenLiftimeInMinutes = Convert.ToInt32(Configuration["TokenAuthentication:AccessTokenLifetimeInMinutes"]);
            var refreshTokenLiftimeInHours = Convert.ToInt32(Configuration["TokenAuthentication:RefreshTokenLifetimeInHours"]);
            
            // Register the OpenIddict services, including the default Entity Framework stores.
            services
                .AddOpenIddict<ApplicationDbContext>()
                .AddMvcBinders()
                .EnableTokenEndpoint("/connect/token")
                .AllowPasswordFlow()
                .AllowRefreshTokenFlow()
                .AllowCustomFlow("urn:ietf:params:oauth:grant-type:facebook_identity_token")
                .UseJsonWebTokens()
                .SetAccessTokenLifetime(TimeSpan.FromMinutes(accessTokenLiftimeInMinutes))
                .SetRefreshTokenLifetime(TimeSpan.FromHours(refreshTokenLiftimeInHours))
                .AddSigningKey(new X509SecurityKey(GetSigningCertificate()))
                // During development, you can disable the HTTPS requirement.
                .DisableHttpsRequirement();
            //  .AddEphemeralSigningKey();
            
            var autofacBuilder = new AutofacConfiguration();
            var builder = autofacBuilder.CreateContainerBuilder(Configuration);       
            builder.Populate(services);
            var container = builder.Build();

            return container.Resolve<IServiceProvider>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();
            loggerFactory.AddSerilog();

            app.UseCors(builder =>
                builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowCredentials());
            
            app.UseOAuthValidation();
            app.UseOpenIddict();

            var audience = Configuration["TokenAuthentication:Audience"];
            var authority = Configuration["TokenAuthentication:Authority"];
                      
            app.UseJwtBearerAuthentication(new JwtBearerOptions
            {
                AutomaticAuthenticate = true,
                AutomaticChallenge = true,
                RequireHttpsMetadata = false,
                Audience = audience,
                Authority = authority, 
                TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new X509SecurityKey(GetSigningCertificate()),                    
                    ValidateIssuer = true,
                    ValidateAudience = false,
                    ValidateLifetime = true,
                    ClockSkew = TimeSpan.Zero                   
                }                
            });

            app.UseMvcWithDefaultRoute();
        }
        
        private X509Certificate2 GetSigningCertificate()
        {
            var certificatPath = Configuration["TokenAuthentication:CertificatePath"];
            var certificatePassword = Configuration["TokenAuthentication:CertificatePassword"];

            return new X509Certificate2(certificatPath, certificatePassword);
        }
    }
}
