﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FarmMachine.Api.Migrations
{
    public partial class CreatedFarmModule : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "FarmAddressCategories",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Main = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FarmAddressCategories", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "FarmPeople",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatorUserId = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    PhoneNo = table.Column<string>(nullable: true),
                    Role = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FarmPeople", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FarmPeople_AspNetUsers_CreatorUserId",
                        column: x => x.CreatorUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Farms",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Active = table.Column<bool>(nullable: false),
                    ContactPersonId = table.Column<Guid>(nullable: true),
                    CreationTime = table.Column<DateTimeOffset>(nullable: false),
                    CreatorUserId = table.Column<string>(nullable: true),
                    FarmIdentifier = table.Column<string>(nullable: true),
                    ModificationTime = table.Column<DateTimeOffset>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    OwnerUserId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Farms", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Farms_FarmPeople_ContactPersonId",
                        column: x => x.ContactPersonId,
                        principalTable: "FarmPeople",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Farms_AspNetUsers_CreatorUserId",
                        column: x => x.CreatorUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "FarmAddresses",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CategoryId = table.Column<Guid>(nullable: false),
                    City = table.Column<string>(nullable: true),
                    Country = table.Column<string>(nullable: true),
                    FarmId = table.Column<Guid>(nullable: true),
                    HomeNo = table.Column<string>(nullable: true),
                    PostCode = table.Column<string>(nullable: true),
                    Street = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FarmAddresses", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FarmAddresses_FarmAddressCategories_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "FarmAddressCategories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_FarmAddresses_Farms_FarmId",
                        column: x => x.FarmId,
                        principalTable: "Farms",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "FarmCategories",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    FarmId = table.Column<Guid>(nullable: true),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FarmCategories", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FarmCategories_Farms_FarmId",
                        column: x => x.FarmId,
                        principalTable: "Farms",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Farms_ContactPersonId",
                table: "Farms",
                column: "ContactPersonId");

            migrationBuilder.CreateIndex(
                name: "IX_Farms_CreatorUserId",
                table: "Farms",
                column: "CreatorUserId");

            migrationBuilder.CreateIndex(
                name: "IX_FarmAddresses_CategoryId",
                table: "FarmAddresses",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_FarmAddresses_FarmId",
                table: "FarmAddresses",
                column: "FarmId");

            migrationBuilder.CreateIndex(
                name: "IX_FarmCategories_FarmId",
                table: "FarmCategories",
                column: "FarmId");

            migrationBuilder.CreateIndex(
                name: "IX_FarmPeople_CreatorUserId",
                table: "FarmPeople",
                column: "CreatorUserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "FarmAddresses");

            migrationBuilder.DropTable(
                name: "FarmCategories");

            migrationBuilder.DropTable(
                name: "FarmAddressCategories");

            migrationBuilder.DropTable(
                name: "Farms");

            migrationBuilder.DropTable(
                name: "FarmPeople");
        }
    }
}
