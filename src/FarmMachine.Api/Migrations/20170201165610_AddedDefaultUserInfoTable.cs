﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FarmMachine.Api.Migrations
{
    public partial class AddedDefaultUserInfoTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AccountingExpenseCattegoryManyRelation");

            migrationBuilder.CreateTable(
                name: "AccountingExpenseCategoryManyRelation",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    AccountingPlannedExpenseId = table.Column<Guid>(nullable: true),
                    ExpenseCategoryId = table.Column<Guid>(nullable: false),
                    ExpenseId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccountingExpenseCategoryManyRelation", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AccountingExpenseCategoryManyRelation_AccountingPlannedExpense_AccountingPlannedExpenseId",
                        column: x => x.AccountingPlannedExpenseId,
                        principalTable: "AccountingPlannedExpense",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AccountingExpenseCategoryManyRelation_AccountingExpenseCategories_ExpenseCategoryId",
                        column: x => x.ExpenseCategoryId,
                        principalTable: "AccountingExpenseCategories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AccountingExpenseCategoryManyRelation_AccountingExpenses_ExpenseId",
                        column: x => x.ExpenseId,
                        principalTable: "AccountingExpenses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DefaultUserInfos",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatorUserId = table.Column<string>(nullable: true),
                    FarmId = table.Column<Guid>(nullable: true),
                    SeasonId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DefaultUserInfos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DefaultUserInfos_AspNetUsers_CreatorUserId",
                        column: x => x.CreatorUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DefaultUserInfos_Farms_FarmId",
                        column: x => x.FarmId,
                        principalTable: "Farms",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DefaultUserInfos_Seasons_SeasonId",
                        column: x => x.SeasonId,
                        principalTable: "Seasons",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AccountingExpenseCategoryManyRelation_AccountingPlannedExpenseId",
                table: "AccountingExpenseCategoryManyRelation",
                column: "AccountingPlannedExpenseId");

            migrationBuilder.CreateIndex(
                name: "IX_AccountingExpenseCategoryManyRelation_ExpenseCategoryId",
                table: "AccountingExpenseCategoryManyRelation",
                column: "ExpenseCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_AccountingExpenseCategoryManyRelation_ExpenseId",
                table: "AccountingExpenseCategoryManyRelation",
                column: "ExpenseId");

            migrationBuilder.CreateIndex(
                name: "IX_DefaultUserInfos_CreatorUserId",
                table: "DefaultUserInfos",
                column: "CreatorUserId");

            migrationBuilder.CreateIndex(
                name: "IX_DefaultUserInfos_FarmId",
                table: "DefaultUserInfos",
                column: "FarmId");

            migrationBuilder.CreateIndex(
                name: "IX_DefaultUserInfos_SeasonId",
                table: "DefaultUserInfos",
                column: "SeasonId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AccountingExpenseCategoryManyRelation");

            migrationBuilder.DropTable(
                name: "DefaultUserInfos");

            migrationBuilder.CreateTable(
                name: "AccountingExpenseCattegoryManyRelation",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    AccountingPlannedExpenseId = table.Column<Guid>(nullable: true),
                    ExpenseCategoryId = table.Column<Guid>(nullable: false),
                    ExpenseId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccountingExpenseCattegoryManyRelation", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AccountingExpenseCattegoryManyRelation_AccountingPlannedExpense_AccountingPlannedExpenseId",
                        column: x => x.AccountingPlannedExpenseId,
                        principalTable: "AccountingPlannedExpense",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AccountingExpenseCattegoryManyRelation_AccountingExpenseCategories_ExpenseCategoryId",
                        column: x => x.ExpenseCategoryId,
                        principalTable: "AccountingExpenseCategories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AccountingExpenseCattegoryManyRelation_AccountingExpenses_ExpenseId",
                        column: x => x.ExpenseId,
                        principalTable: "AccountingExpenses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AccountingExpenseCattegoryManyRelation_AccountingPlannedExpenseId",
                table: "AccountingExpenseCattegoryManyRelation",
                column: "AccountingPlannedExpenseId");

            migrationBuilder.CreateIndex(
                name: "IX_AccountingExpenseCattegoryManyRelation_ExpenseCategoryId",
                table: "AccountingExpenseCattegoryManyRelation",
                column: "ExpenseCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_AccountingExpenseCattegoryManyRelation_ExpenseId",
                table: "AccountingExpenseCattegoryManyRelation",
                column: "ExpenseId");
        }
    }
}
