﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using FarmMachine.Models.Database;
using FarmMachine.Models.Enums;

namespace FarmMachine.Api.Migrations
{
    [DbContext(typeof(ApplicationDbContext))]
    partial class ApplicationDbContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.0-rtm-22752")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("FarmMachine.Models.Entities.AccountingCorrectionInvoice", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Comment");

                    b.Property<DateTimeOffset>("CreationTime");

                    b.Property<string>("CreatorUserId");

                    b.Property<Guid?>("FarmId");

                    b.Property<decimal>("GrossSum");

                    b.Property<Guid>("InvoiceId");

                    b.Property<DateTimeOffset>("IssueDate");

                    b.Property<DateTimeOffset>("ModificationTime");

                    b.Property<decimal>("NetSum");

                    b.Property<DateTimeOffset>("PaymentDate");

                    b.Property<DateTimeOffset>("SaleDate");

                    b.HasKey("Id");

                    b.HasIndex("CreatorUserId");

                    b.HasIndex("FarmId");

                    b.HasIndex("InvoiceId");

                    b.ToTable("AccountingCorrectionInvoices");
                });

            modelBuilder.Entity("FarmMachine.Models.Entities.AccountingExpense", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTimeOffset>("CreationTime");

                    b.Property<string>("CreatorUserId");

                    b.Property<DateTimeOffset>("Date");

                    b.Property<string>("Description");

                    b.Property<Guid?>("FarmId");

                    b.Property<decimal>("GrossValue");

                    b.Property<DateTimeOffset>("ModificationTime");

                    b.Property<string>("Name");

                    b.Property<decimal>("NetValue");

                    b.Property<bool>("Planned");

                    b.Property<Guid?>("PlannedExpenseId");

                    b.Property<Guid>("SeasonId");

                    b.HasKey("Id");

                    b.HasIndex("CreatorUserId");

                    b.HasIndex("FarmId");

                    b.HasIndex("PlannedExpenseId");

                    b.HasIndex("SeasonId");

                    b.ToTable("AccountingExpenses");
                });

            modelBuilder.Entity("FarmMachine.Models.Entities.AccountingExpenseCategory", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("AccountingExpenseCategories");
                });

            modelBuilder.Entity("FarmMachine.Models.Entities.AccountingExpenseCategoryManyRelation", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid?>("AccountingPlannedExpenseId");

                    b.Property<Guid>("ExpenseCategoryId");

                    b.Property<Guid>("ExpenseId");

                    b.HasKey("Id");

                    b.HasIndex("AccountingPlannedExpenseId");

                    b.HasIndex("ExpenseCategoryId");

                    b.HasIndex("ExpenseId");

                    b.ToTable("AccountingExpenseCategoryManyRelation");
                });

            modelBuilder.Entity("FarmMachine.Models.Entities.AccountingInfo", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CompanyAccountNo");

                    b.Property<string>("CompanyAddressCity");

                    b.Property<string>("CompanyAddressHomeNo");

                    b.Property<string>("CompanyAddressPostCode");

                    b.Property<string>("CompanyAddressStreet");

                    b.Property<string>("CompanyName");

                    b.Property<DateTimeOffset>("CreationTime");

                    b.Property<string>("CreatorUserId");

                    b.Property<bool>("Default");

                    b.Property<Guid?>("FarmId");

                    b.Property<DateTimeOffset>("ModificationTime");

                    b.Property<bool>("VatPayer");

                    b.Property<string>("VatRegistrationNumber");

                    b.HasKey("Id");

                    b.HasIndex("CreatorUserId");

                    b.HasIndex("FarmId");

                    b.ToTable("AccountingInfos");
                });

            modelBuilder.Entity("FarmMachine.Models.Entities.AccountingInvoice", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid?>("AccountingInfoId");

                    b.Property<DateTimeOffset>("CreationTime");

                    b.Property<string>("CreatorUserId");

                    b.Property<Guid?>("FarmId");

                    b.Property<string>("FromCompanyAccountNo");

                    b.Property<string>("FromCompanyAddressCity");

                    b.Property<string>("FromCompanyAddressHomeNo");

                    b.Property<string>("FromCompanyAddressPostCode");

                    b.Property<string>("FromCompanyAddressStreet");

                    b.Property<string>("FromCompanyName");

                    b.Property<string>("FromCompanyVatRegistrationNumber");

                    b.Property<decimal>("GrossSum");

                    b.Property<DateTimeOffset>("IssueDate");

                    b.Property<DateTimeOffset>("ModificationTime");

                    b.Property<decimal>("NetSum");

                    b.Property<bool>("Paid");

                    b.Property<DateTimeOffset>("PaymentDate");

                    b.Property<DateTimeOffset>("SaleDate");

                    b.Property<Guid>("SeasonId");

                    b.Property<string>("ToCompanyAddressCity");

                    b.Property<string>("ToCompanyAddressHomeNo");

                    b.Property<string>("ToCompanyAddressPostCode");

                    b.Property<string>("ToCompanyAddressStreet");

                    b.Property<string>("ToCompanyName");

                    b.Property<string>("ToCompanyVatRegistrationNumber");

                    b.HasKey("Id");

                    b.HasIndex("AccountingInfoId");

                    b.HasIndex("CreatorUserId");

                    b.HasIndex("FarmId");

                    b.HasIndex("SeasonId");

                    b.ToTable("AccountingInvoices");
                });

            modelBuilder.Entity("FarmMachine.Models.Entities.AccountingInvoiceItem", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid?>("AccountingCorrectionInvoiceId");

                    b.Property<Guid?>("AccountingInvoiceId");

                    b.Property<int>("Amount");

                    b.Property<decimal>("GrossValue");

                    b.Property<string>("Name");

                    b.Property<decimal>("NetValue");

                    b.Property<string>("Pkuiw");

                    b.Property<decimal>("UnitValue");

                    b.Property<int>("VatValue");

                    b.HasKey("Id");

                    b.HasIndex("AccountingCorrectionInvoiceId");

                    b.HasIndex("AccountingInvoiceId");

                    b.ToTable("AccountingInvoiceItems");
                });

            modelBuilder.Entity("FarmMachine.Models.Entities.AccountingPlannedExpense", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTimeOffset>("CreationTime");

                    b.Property<string>("CreatorUserId");

                    b.Property<DateTimeOffset>("Date");

                    b.Property<DateTimeOffset>("DateFrom");

                    b.Property<DateTimeOffset>("DateTo");

                    b.Property<string>("Description");

                    b.Property<Guid?>("FarmId");

                    b.Property<decimal>("GrossValue");

                    b.Property<DateTimeOffset>("ModificationTime");

                    b.Property<string>("Name");

                    b.Property<decimal>("NetValue");

                    b.HasKey("Id");

                    b.HasIndex("CreatorUserId");

                    b.HasIndex("FarmId");

                    b.ToTable("AccountingPlannedExpense");
                });

            modelBuilder.Entity("FarmMachine.Models.Entities.AccountingPlannedRevenue", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTimeOffset>("CreationTime");

                    b.Property<string>("CreatorUserId");

                    b.Property<DateTimeOffset>("Date");

                    b.Property<DateTimeOffset>("DateFrom");

                    b.Property<DateTimeOffset>("DateTo");

                    b.Property<string>("Description");

                    b.Property<Guid?>("FarmId");

                    b.Property<decimal>("GrossValue");

                    b.Property<DateTimeOffset>("ModificationTime");

                    b.Property<string>("Name");

                    b.Property<decimal>("NetValue");

                    b.HasKey("Id");

                    b.HasIndex("CreatorUserId");

                    b.HasIndex("FarmId");

                    b.ToTable("AccountingPlannedRevenue");
                });

            modelBuilder.Entity("FarmMachine.Models.Entities.AccountingRevenue", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTimeOffset>("CreationTime");

                    b.Property<string>("CreatorUserId");

                    b.Property<DateTimeOffset>("Date");

                    b.Property<string>("Description");

                    b.Property<Guid?>("FarmId");

                    b.Property<decimal>("GrossValue");

                    b.Property<Guid?>("InvoiceId");

                    b.Property<DateTimeOffset>("ModificationTime");

                    b.Property<string>("Name");

                    b.Property<decimal>("NetValue");

                    b.Property<bool>("Planned");

                    b.Property<Guid?>("PlannedRevenueId");

                    b.Property<Guid>("SeasonId");

                    b.HasKey("Id");

                    b.HasIndex("CreatorUserId");

                    b.HasIndex("FarmId");

                    b.HasIndex("InvoiceId");

                    b.HasIndex("PlannedRevenueId");

                    b.HasIndex("SeasonId");

                    b.ToTable("AccountingRevenues");
                });

            modelBuilder.Entity("FarmMachine.Models.Entities.AccountingRevenueCategory", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("AccountingRevenueCategories");
                });

            modelBuilder.Entity("FarmMachine.Models.Entities.AccountingRevenueCategoryManyRelation", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid>("RevenueCategoryId");

                    b.Property<Guid>("RevenueId");

                    b.HasKey("Id");

                    b.HasIndex("RevenueCategoryId");

                    b.HasIndex("RevenueId");

                    b.ToTable("AccountingRevenueCategoryManyRelation");
                });

            modelBuilder.Entity("FarmMachine.Models.Entities.ActionNotification", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Content");

                    b.Property<DateTimeOffset>("CreationTime");

                    b.Property<string>("CreatorUserId");

                    b.Property<Guid?>("FarmId");

                    b.Property<Guid?>("ObjectId");

                    b.Property<string>("Title");

                    b.Property<int>("Type");

                    b.HasKey("Id");

                    b.HasIndex("CreatorUserId");

                    b.HasIndex("FarmId");

                    b.ToTable("ActionNotifications");
                });

            modelBuilder.Entity("FarmMachine.Models.Entities.ApplicationUser", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AccessFailedCount");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Email")
                        .HasMaxLength(256);

                    b.Property<bool>("EmailConfirmed");

                    b.Property<bool>("LockoutEnabled");

                    b.Property<DateTimeOffset?>("LockoutEnd");

                    b.Property<string>("NormalizedEmail")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedUserName")
                        .HasMaxLength(256);

                    b.Property<string>("PasswordHash");

                    b.Property<string>("PhoneNumber");

                    b.Property<bool>("PhoneNumberConfirmed");

                    b.Property<string>("SecurityStamp");

                    b.Property<bool>("TwoFactorEnabled");

                    b.Property<string>("UserName")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedEmail")
                        .HasName("EmailIndex");

                    b.HasIndex("NormalizedUserName")
                        .IsUnique()
                        .HasName("UserNameIndex");

                    b.ToTable("AspNetUsers");
                });

            modelBuilder.Entity("FarmMachine.Models.Entities.DefaultUserInfo", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatorUserId");

                    b.Property<Guid?>("FarmId");

                    b.Property<Guid?>("SeasonId");

                    b.HasKey("Id");

                    b.HasIndex("CreatorUserId");

                    b.HasIndex("FarmId");

                    b.HasIndex("SeasonId");

                    b.ToTable("DefaultUserInfos");
                });

            modelBuilder.Entity("FarmMachine.Models.Entities.Farm", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("Active");

                    b.Property<Guid?>("ContactPersonId");

                    b.Property<DateTimeOffset>("CreationTime");

                    b.Property<string>("CreatorUserId");

                    b.Property<bool>("Deleted");

                    b.Property<string>("FarmIdentifier");

                    b.Property<DateTimeOffset>("ModificationTime");

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.HasIndex("ContactPersonId");

                    b.HasIndex("CreatorUserId");

                    b.ToTable("Farms");
                });

            modelBuilder.Entity("FarmMachine.Models.Entities.FarmAddress", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("City");

                    b.Property<string>("Country");

                    b.Property<Guid?>("FarmId");

                    b.Property<string>("HomeNo");

                    b.Property<bool>("Main");

                    b.Property<string>("PostCode");

                    b.Property<string>("Street");

                    b.HasKey("Id");

                    b.HasIndex("FarmId");

                    b.ToTable("FarmAddresses");
                });

            modelBuilder.Entity("FarmMachine.Models.Entities.FarmCategory", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("FarmCategories");
                });

            modelBuilder.Entity("FarmMachine.Models.Entities.FarmCategoryManyRelation", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid>("FarmCategoryId");

                    b.Property<Guid>("FarmId");

                    b.HasKey("Id");

                    b.HasIndex("FarmCategoryId");

                    b.HasIndex("FarmId");

                    b.ToTable("FarmCategoryManyRelation");
                });

            modelBuilder.Entity("FarmMachine.Models.Entities.FarmPerson", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatorUserId");

                    b.Property<string>("Email");

                    b.Property<string>("FirstName");

                    b.Property<string>("LastName");

                    b.Property<string>("PhoneNo");

                    b.Property<string>("Role");

                    b.HasKey("Id");

                    b.HasIndex("CreatorUserId");

                    b.ToTable("FarmPeople");
                });

            modelBuilder.Entity("FarmMachine.Models.Entities.Season", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTimeOffset>("CreationTime");

                    b.Property<string>("CreatorUserId");

                    b.Property<DateTimeOffset>("DateFrom");

                    b.Property<DateTimeOffset>("DateTo");

                    b.Property<int>("Days");

                    b.Property<bool>("Deleted");

                    b.Property<int>("FarmCount");

                    b.Property<DateTimeOffset>("ModificationTime");

                    b.HasKey("Id");

                    b.HasIndex("CreatorUserId");

                    b.ToTable("Seasons");
                });

            modelBuilder.Entity("FarmMachine.Models.Entities.SeasonFarmManyRelation", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid>("FarmId");

                    b.Property<Guid>("SeasonId");

                    b.HasKey("Id");

                    b.HasIndex("FarmId");

                    b.HasIndex("SeasonId");

                    b.ToTable("SeasonFarmManyRelation");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRole", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Name")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedName")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedName")
                        .IsUnique()
                        .HasName("RoleNameIndex");

                    b.ToTable("AspNetRoles");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRoleClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("RoleId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetRoleClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserLogin<string>", b =>
                {
                    b.Property<string>("LoginProvider");

                    b.Property<string>("ProviderKey");

                    b.Property<string>("ProviderDisplayName");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("LoginProvider", "ProviderKey");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserLogins");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserRole<string>", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("RoleId");

                    b.HasKey("UserId", "RoleId");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetUserRoles");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserToken<string>", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("LoginProvider");

                    b.Property<string>("Name");

                    b.Property<string>("Value");

                    b.HasKey("UserId", "LoginProvider", "Name");

                    b.ToTable("AspNetUserTokens");
                });

            modelBuilder.Entity("OpenIddict.OpenIddictApplication", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClientId");

                    b.Property<string>("ClientSecret");

                    b.Property<string>("DisplayName");

                    b.Property<string>("LogoutRedirectUri");

                    b.Property<string>("RedirectUri");

                    b.Property<string>("Type");

                    b.HasKey("Id");

                    b.HasIndex("ClientId")
                        .IsUnique();

                    b.ToTable("OpenIddictApplications");
                });

            modelBuilder.Entity("OpenIddict.OpenIddictAuthorization", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Scope");

                    b.HasKey("Id");

                    b.ToTable("OpenIddictAuthorizations");
                });

            modelBuilder.Entity("OpenIddict.OpenIddictScope", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Description");

                    b.HasKey("Id");

                    b.ToTable("OpenIddictScopes");
                });

            modelBuilder.Entity("OpenIddict.OpenIddictToken", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ApplicationId");

                    b.Property<string>("AuthorizationId");

                    b.Property<string>("Type");

                    b.HasKey("Id");

                    b.HasIndex("ApplicationId");

                    b.HasIndex("AuthorizationId");

                    b.ToTable("OpenIddictTokens");
                });

            modelBuilder.Entity("FarmMachine.Models.Entities.AccountingCorrectionInvoice", b =>
                {
                    b.HasOne("FarmMachine.Models.Entities.ApplicationUser", "CreatorUser")
                        .WithMany()
                        .HasForeignKey("CreatorUserId");

                    b.HasOne("FarmMachine.Models.Entities.Farm", "Farm")
                        .WithMany()
                        .HasForeignKey("FarmId");

                    b.HasOne("FarmMachine.Models.Entities.AccountingInvoice", "Invoice")
                        .WithMany()
                        .HasForeignKey("InvoiceId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("FarmMachine.Models.Entities.AccountingExpense", b =>
                {
                    b.HasOne("FarmMachine.Models.Entities.ApplicationUser", "CreatorUser")
                        .WithMany()
                        .HasForeignKey("CreatorUserId");

                    b.HasOne("FarmMachine.Models.Entities.Farm", "Farm")
                        .WithMany()
                        .HasForeignKey("FarmId");

                    b.HasOne("FarmMachine.Models.Entities.AccountingPlannedExpense", "PlannedExpense")
                        .WithMany()
                        .HasForeignKey("PlannedExpenseId");

                    b.HasOne("FarmMachine.Models.Entities.Season", "Season")
                        .WithMany()
                        .HasForeignKey("SeasonId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("FarmMachine.Models.Entities.AccountingExpenseCategoryManyRelation", b =>
                {
                    b.HasOne("FarmMachine.Models.Entities.AccountingPlannedExpense")
                        .WithMany("ExpenseCategories")
                        .HasForeignKey("AccountingPlannedExpenseId");

                    b.HasOne("FarmMachine.Models.Entities.AccountingExpenseCategory", "ExpenseCategory")
                        .WithMany()
                        .HasForeignKey("ExpenseCategoryId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("FarmMachine.Models.Entities.AccountingExpense", "Expense")
                        .WithMany("ExpenseCategories")
                        .HasForeignKey("ExpenseId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("FarmMachine.Models.Entities.AccountingInfo", b =>
                {
                    b.HasOne("FarmMachine.Models.Entities.ApplicationUser", "CreatorUser")
                        .WithMany()
                        .HasForeignKey("CreatorUserId");

                    b.HasOne("FarmMachine.Models.Entities.Farm", "Farm")
                        .WithMany()
                        .HasForeignKey("FarmId");
                });

            modelBuilder.Entity("FarmMachine.Models.Entities.AccountingInvoice", b =>
                {
                    b.HasOne("FarmMachine.Models.Entities.AccountingInfo", "AccountingInfo")
                        .WithMany()
                        .HasForeignKey("AccountingInfoId");

                    b.HasOne("FarmMachine.Models.Entities.ApplicationUser", "CreatorUser")
                        .WithMany()
                        .HasForeignKey("CreatorUserId");

                    b.HasOne("FarmMachine.Models.Entities.Farm", "Farm")
                        .WithMany()
                        .HasForeignKey("FarmId");

                    b.HasOne("FarmMachine.Models.Entities.Season", "Season")
                        .WithMany()
                        .HasForeignKey("SeasonId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("FarmMachine.Models.Entities.AccountingInvoiceItem", b =>
                {
                    b.HasOne("FarmMachine.Models.Entities.AccountingCorrectionInvoice")
                        .WithMany("Items")
                        .HasForeignKey("AccountingCorrectionInvoiceId");

                    b.HasOne("FarmMachine.Models.Entities.AccountingInvoice")
                        .WithMany("Items")
                        .HasForeignKey("AccountingInvoiceId");
                });

            modelBuilder.Entity("FarmMachine.Models.Entities.AccountingPlannedExpense", b =>
                {
                    b.HasOne("FarmMachine.Models.Entities.ApplicationUser", "CreatorUser")
                        .WithMany()
                        .HasForeignKey("CreatorUserId");

                    b.HasOne("FarmMachine.Models.Entities.Farm", "Farm")
                        .WithMany()
                        .HasForeignKey("FarmId");
                });

            modelBuilder.Entity("FarmMachine.Models.Entities.AccountingPlannedRevenue", b =>
                {
                    b.HasOne("FarmMachine.Models.Entities.ApplicationUser", "CreatorUser")
                        .WithMany()
                        .HasForeignKey("CreatorUserId");

                    b.HasOne("FarmMachine.Models.Entities.Farm", "Farm")
                        .WithMany()
                        .HasForeignKey("FarmId");
                });

            modelBuilder.Entity("FarmMachine.Models.Entities.AccountingRevenue", b =>
                {
                    b.HasOne("FarmMachine.Models.Entities.ApplicationUser", "CreatorUser")
                        .WithMany()
                        .HasForeignKey("CreatorUserId");

                    b.HasOne("FarmMachine.Models.Entities.Farm", "Farm")
                        .WithMany()
                        .HasForeignKey("FarmId");

                    b.HasOne("FarmMachine.Models.Entities.AccountingInvoice", "Invoice")
                        .WithMany()
                        .HasForeignKey("InvoiceId");

                    b.HasOne("FarmMachine.Models.Entities.AccountingPlannedRevenue", "PlannedRevenue")
                        .WithMany()
                        .HasForeignKey("PlannedRevenueId");

                    b.HasOne("FarmMachine.Models.Entities.Season", "Season")
                        .WithMany()
                        .HasForeignKey("SeasonId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("FarmMachine.Models.Entities.AccountingRevenueCategoryManyRelation", b =>
                {
                    b.HasOne("FarmMachine.Models.Entities.AccountingRevenueCategory", "RevenueCategory")
                        .WithMany()
                        .HasForeignKey("RevenueCategoryId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("FarmMachine.Models.Entities.AccountingRevenue", "Revenue")
                        .WithMany("RevenueCategories")
                        .HasForeignKey("RevenueId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("FarmMachine.Models.Entities.ActionNotification", b =>
                {
                    b.HasOne("FarmMachine.Models.Entities.ApplicationUser", "CreatorUser")
                        .WithMany()
                        .HasForeignKey("CreatorUserId");

                    b.HasOne("FarmMachine.Models.Entities.Farm", "Farm")
                        .WithMany()
                        .HasForeignKey("FarmId");
                });

            modelBuilder.Entity("FarmMachine.Models.Entities.DefaultUserInfo", b =>
                {
                    b.HasOne("FarmMachine.Models.Entities.ApplicationUser", "CreatorUser")
                        .WithMany()
                        .HasForeignKey("CreatorUserId");

                    b.HasOne("FarmMachine.Models.Entities.Farm", "Farm")
                        .WithMany()
                        .HasForeignKey("FarmId");

                    b.HasOne("FarmMachine.Models.Entities.Season", "Season")
                        .WithMany()
                        .HasForeignKey("SeasonId");
                });

            modelBuilder.Entity("FarmMachine.Models.Entities.Farm", b =>
                {
                    b.HasOne("FarmMachine.Models.Entities.FarmPerson", "ContactPerson")
                        .WithMany()
                        .HasForeignKey("ContactPersonId");

                    b.HasOne("FarmMachine.Models.Entities.ApplicationUser", "CreatorUser")
                        .WithMany()
                        .HasForeignKey("CreatorUserId");
                });

            modelBuilder.Entity("FarmMachine.Models.Entities.FarmAddress", b =>
                {
                    b.HasOne("FarmMachine.Models.Entities.Farm")
                        .WithMany("Addresses")
                        .HasForeignKey("FarmId");
                });

            modelBuilder.Entity("FarmMachine.Models.Entities.FarmCategoryManyRelation", b =>
                {
                    b.HasOne("FarmMachine.Models.Entities.FarmCategory", "FarmCategory")
                        .WithMany("FarmCategories")
                        .HasForeignKey("FarmCategoryId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("FarmMachine.Models.Entities.Farm", "Farm")
                        .WithMany("FarmCategories")
                        .HasForeignKey("FarmId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("FarmMachine.Models.Entities.FarmPerson", b =>
                {
                    b.HasOne("FarmMachine.Models.Entities.ApplicationUser", "CreatorUser")
                        .WithMany()
                        .HasForeignKey("CreatorUserId");
                });

            modelBuilder.Entity("FarmMachine.Models.Entities.Season", b =>
                {
                    b.HasOne("FarmMachine.Models.Entities.ApplicationUser", "CreatorUser")
                        .WithMany()
                        .HasForeignKey("CreatorUserId");
                });

            modelBuilder.Entity("FarmMachine.Models.Entities.SeasonFarmManyRelation", b =>
                {
                    b.HasOne("FarmMachine.Models.Entities.Farm", "Farm")
                        .WithMany()
                        .HasForeignKey("FarmId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("FarmMachine.Models.Entities.Season", "Season")
                        .WithMany("Farms")
                        .HasForeignKey("SeasonId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRoleClaim<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRole")
                        .WithMany("Claims")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserClaim<string>", b =>
                {
                    b.HasOne("FarmMachine.Models.Entities.ApplicationUser")
                        .WithMany("Claims")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserLogin<string>", b =>
                {
                    b.HasOne("FarmMachine.Models.Entities.ApplicationUser")
                        .WithMany("Logins")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserRole<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRole")
                        .WithMany("Users")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("FarmMachine.Models.Entities.ApplicationUser")
                        .WithMany("Roles")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("OpenIddict.OpenIddictToken", b =>
                {
                    b.HasOne("OpenIddict.OpenIddictApplication")
                        .WithMany("Tokens")
                        .HasForeignKey("ApplicationId");

                    b.HasOne("OpenIddict.OpenIddictAuthorization")
                        .WithMany("Tokens")
                        .HasForeignKey("AuthorizationId");
                });
        }
    }
}
