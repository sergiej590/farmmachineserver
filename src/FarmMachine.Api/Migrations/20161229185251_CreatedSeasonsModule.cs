﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FarmMachine.Api.Migrations
{
    public partial class CreatedSeasonsModule : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "SeasonId",
                table: "AccountingRevenues",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "SeasonId",
                table: "AccountingInvoices",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "SeasonId",
                table: "AccountingExpenses",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateTable(
                name: "Seasons",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreationTime = table.Column<DateTimeOffset>(nullable: false),
                    CreatorUserId = table.Column<string>(nullable: true),
                    DateFrom = table.Column<DateTimeOffset>(nullable: false),
                    DateTo = table.Column<DateTimeOffset>(nullable: false),
                    ModificationTime = table.Column<DateTimeOffset>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Seasons", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Seasons_AspNetUsers_CreatorUserId",
                        column: x => x.CreatorUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SeasonFarmManyRelation",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    FarmId = table.Column<Guid>(nullable: false),
                    SeasonId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SeasonFarmManyRelation", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SeasonFarmManyRelation_Farms_FarmId",
                        column: x => x.FarmId,
                        principalTable: "Farms",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SeasonFarmManyRelation_Seasons_SeasonId",
                        column: x => x.SeasonId,
                        principalTable: "Seasons",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AccountingRevenues_SeasonId",
                table: "AccountingRevenues",
                column: "SeasonId");

            migrationBuilder.CreateIndex(
                name: "IX_AccountingInvoices_SeasonId",
                table: "AccountingInvoices",
                column: "SeasonId");

            migrationBuilder.CreateIndex(
                name: "IX_AccountingExpenses_SeasonId",
                table: "AccountingExpenses",
                column: "SeasonId");

            migrationBuilder.CreateIndex(
                name: "IX_Seasons_CreatorUserId",
                table: "Seasons",
                column: "CreatorUserId");

            migrationBuilder.CreateIndex(
                name: "IX_SeasonFarmManyRelation_FarmId",
                table: "SeasonFarmManyRelation",
                column: "FarmId");

            migrationBuilder.CreateIndex(
                name: "IX_SeasonFarmManyRelation_SeasonId",
                table: "SeasonFarmManyRelation",
                column: "SeasonId");

            migrationBuilder.AddForeignKey(
                name: "FK_AccountingExpenses_Seasons_SeasonId",
                table: "AccountingExpenses",
                column: "SeasonId",
                principalTable: "Seasons",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_AccountingInvoices_Seasons_SeasonId",
                table: "AccountingInvoices",
                column: "SeasonId",
                principalTable: "Seasons",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_AccountingRevenues_Seasons_SeasonId",
                table: "AccountingRevenues",
                column: "SeasonId",
                principalTable: "Seasons",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AccountingExpenses_Seasons_SeasonId",
                table: "AccountingExpenses");

            migrationBuilder.DropForeignKey(
                name: "FK_AccountingInvoices_Seasons_SeasonId",
                table: "AccountingInvoices");

            migrationBuilder.DropForeignKey(
                name: "FK_AccountingRevenues_Seasons_SeasonId",
                table: "AccountingRevenues");

            migrationBuilder.DropTable(
                name: "SeasonFarmManyRelation");

            migrationBuilder.DropTable(
                name: "Seasons");

            migrationBuilder.DropIndex(
                name: "IX_AccountingRevenues_SeasonId",
                table: "AccountingRevenues");

            migrationBuilder.DropIndex(
                name: "IX_AccountingInvoices_SeasonId",
                table: "AccountingInvoices");

            migrationBuilder.DropIndex(
                name: "IX_AccountingExpenses_SeasonId",
                table: "AccountingExpenses");

            migrationBuilder.DropColumn(
                name: "SeasonId",
                table: "AccountingRevenues");

            migrationBuilder.DropColumn(
                name: "SeasonId",
                table: "AccountingInvoices");

            migrationBuilder.DropColumn(
                name: "SeasonId",
                table: "AccountingExpenses");
        }
    }
}
