﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FarmMachine.Api.Migrations
{
    public partial class CreatedAccountingModule : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AccountingExpenseCategories",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccountingExpenseCategories", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AccountingInfos",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CompanyAccountNo = table.Column<string>(nullable: true),
                    CompanyAddressCity = table.Column<string>(nullable: true),
                    CompanyAddressHomeNo = table.Column<string>(nullable: true),
                    CompanyAddressPostCode = table.Column<string>(nullable: true),
                    CompanyAddressStreet = table.Column<string>(nullable: true),
                    CompanyName = table.Column<string>(nullable: true),
                    CreationTime = table.Column<DateTimeOffset>(nullable: false),
                    CreatorUserId = table.Column<string>(nullable: true),
                    Default = table.Column<bool>(nullable: false),
                    FarmId = table.Column<Guid>(nullable: true),
                    ModificationTime = table.Column<DateTimeOffset>(nullable: false),
                    VatPayer = table.Column<bool>(nullable: false),
                    VatRegistrationNumber = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccountingInfos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AccountingInfos_AspNetUsers_CreatorUserId",
                        column: x => x.CreatorUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AccountingInfos_Farms_FarmId",
                        column: x => x.FarmId,
                        principalTable: "Farms",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AccountingPlannedExpense",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreationTime = table.Column<DateTimeOffset>(nullable: false),
                    CreatorUserId = table.Column<string>(nullable: true),
                    Date = table.Column<DateTimeOffset>(nullable: false),
                    DateFrom = table.Column<DateTimeOffset>(nullable: false),
                    DateTo = table.Column<DateTimeOffset>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    FarmId = table.Column<Guid>(nullable: true),
                    GrossValue = table.Column<decimal>(nullable: false),
                    ModificationTime = table.Column<DateTimeOffset>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    NetValue = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccountingPlannedExpense", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AccountingPlannedExpense_AspNetUsers_CreatorUserId",
                        column: x => x.CreatorUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AccountingPlannedExpense_Farms_FarmId",
                        column: x => x.FarmId,
                        principalTable: "Farms",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AccountingPlannedRevenue",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreationTime = table.Column<DateTimeOffset>(nullable: false),
                    CreatorUserId = table.Column<string>(nullable: true),
                    Date = table.Column<DateTimeOffset>(nullable: false),
                    DateFrom = table.Column<DateTimeOffset>(nullable: false),
                    DateTo = table.Column<DateTimeOffset>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    FarmId = table.Column<Guid>(nullable: true),
                    GrossValue = table.Column<decimal>(nullable: false),
                    ModificationTime = table.Column<DateTimeOffset>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    NetValue = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccountingPlannedRevenue", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AccountingPlannedRevenue_AspNetUsers_CreatorUserId",
                        column: x => x.CreatorUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AccountingPlannedRevenue_Farms_FarmId",
                        column: x => x.FarmId,
                        principalTable: "Farms",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AccountingRevenueCategories",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccountingRevenueCategories", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AccountingInvoices",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    AccountingInfoId = table.Column<Guid>(nullable: true),
                    CreationTime = table.Column<DateTimeOffset>(nullable: false),
                    CreatorUserId = table.Column<string>(nullable: true),
                    FarmId = table.Column<Guid>(nullable: true),
                    FromCompanyAccountNo = table.Column<string>(nullable: true),
                    FromCompanyAddressCity = table.Column<string>(nullable: true),
                    FromCompanyAddressHomeNo = table.Column<string>(nullable: true),
                    FromCompanyAddressPostCode = table.Column<string>(nullable: true),
                    FromCompanyAddressStreet = table.Column<string>(nullable: true),
                    FromCompanyName = table.Column<string>(nullable: true),
                    FromCompanyVatRegistrationNumber = table.Column<string>(nullable: true),
                    GrossSum = table.Column<decimal>(nullable: false),
                    IssueDate = table.Column<DateTimeOffset>(nullable: false),
                    ModificationTime = table.Column<DateTimeOffset>(nullable: false),
                    NetSum = table.Column<decimal>(nullable: false),
                    Paid = table.Column<bool>(nullable: false),
                    PaymentDate = table.Column<DateTimeOffset>(nullable: false),
                    SaleDate = table.Column<DateTimeOffset>(nullable: false),
                    ToCompanyAddressCity = table.Column<string>(nullable: true),
                    ToCompanyAddressHomeNo = table.Column<string>(nullable: true),
                    ToCompanyAddressPostCode = table.Column<string>(nullable: true),
                    ToCompanyAddressStreet = table.Column<string>(nullable: true),
                    ToCompanyName = table.Column<string>(nullable: true),
                    ToCompanyVatRegistrationNumber = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccountingInvoices", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AccountingInvoices_AccountingInfos_AccountingInfoId",
                        column: x => x.AccountingInfoId,
                        principalTable: "AccountingInfos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AccountingInvoices_AspNetUsers_CreatorUserId",
                        column: x => x.CreatorUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AccountingInvoices_Farms_FarmId",
                        column: x => x.FarmId,
                        principalTable: "Farms",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AccountingExpenses",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreationTime = table.Column<DateTimeOffset>(nullable: false),
                    CreatorUserId = table.Column<string>(nullable: true),
                    Date = table.Column<DateTimeOffset>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    FarmId = table.Column<Guid>(nullable: true),
                    GrossValue = table.Column<decimal>(nullable: false),
                    ModificationTime = table.Column<DateTimeOffset>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    NetValue = table.Column<decimal>(nullable: false),
                    Planned = table.Column<bool>(nullable: false),
                    PlannedExpenseId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccountingExpenses", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AccountingExpenses_AspNetUsers_CreatorUserId",
                        column: x => x.CreatorUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AccountingExpenses_Farms_FarmId",
                        column: x => x.FarmId,
                        principalTable: "Farms",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AccountingExpenses_AccountingPlannedExpense_PlannedExpenseId",
                        column: x => x.PlannedExpenseId,
                        principalTable: "AccountingPlannedExpense",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AccountingCorrectionInvoices",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Comment = table.Column<string>(nullable: true),
                    CreationTime = table.Column<DateTimeOffset>(nullable: false),
                    CreatorUserId = table.Column<string>(nullable: true),
                    FarmId = table.Column<Guid>(nullable: true),
                    GrossSum = table.Column<decimal>(nullable: false),
                    InvoiceId = table.Column<Guid>(nullable: false),
                    IssueDate = table.Column<DateTimeOffset>(nullable: false),
                    ModificationTime = table.Column<DateTimeOffset>(nullable: false),
                    NetSum = table.Column<decimal>(nullable: false),
                    PaymentDate = table.Column<DateTimeOffset>(nullable: false),
                    SaleDate = table.Column<DateTimeOffset>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccountingCorrectionInvoices", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AccountingCorrectionInvoices_AspNetUsers_CreatorUserId",
                        column: x => x.CreatorUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AccountingCorrectionInvoices_Farms_FarmId",
                        column: x => x.FarmId,
                        principalTable: "Farms",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AccountingCorrectionInvoices_AccountingInvoices_InvoiceId",
                        column: x => x.InvoiceId,
                        principalTable: "AccountingInvoices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AccountingRevenues",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreationTime = table.Column<DateTimeOffset>(nullable: false),
                    CreatorUserId = table.Column<string>(nullable: true),
                    Date = table.Column<DateTimeOffset>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    FarmId = table.Column<Guid>(nullable: true),
                    GrossValue = table.Column<decimal>(nullable: false),
                    InvoiceId = table.Column<Guid>(nullable: true),
                    ModificationTime = table.Column<DateTimeOffset>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    NetValue = table.Column<decimal>(nullable: false),
                    Planned = table.Column<bool>(nullable: false),
                    PlannedRevenueId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccountingRevenues", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AccountingRevenues_AspNetUsers_CreatorUserId",
                        column: x => x.CreatorUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AccountingRevenues_Farms_FarmId",
                        column: x => x.FarmId,
                        principalTable: "Farms",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AccountingRevenues_AccountingInvoices_InvoiceId",
                        column: x => x.InvoiceId,
                        principalTable: "AccountingInvoices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AccountingRevenues_AccountingPlannedRevenue_PlannedRevenueId",
                        column: x => x.PlannedRevenueId,
                        principalTable: "AccountingPlannedRevenue",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AccountingExpenseCattegoryManyRelation",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    AccountingPlannedExpenseId = table.Column<Guid>(nullable: true),
                    ExpenseCategoryId = table.Column<Guid>(nullable: false),
                    ExpenseId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccountingExpenseCattegoryManyRelation", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AccountingExpenseCattegoryManyRelation_AccountingPlannedExpense_AccountingPlannedExpenseId",
                        column: x => x.AccountingPlannedExpenseId,
                        principalTable: "AccountingPlannedExpense",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AccountingExpenseCattegoryManyRelation_AccountingExpenseCategories_ExpenseCategoryId",
                        column: x => x.ExpenseCategoryId,
                        principalTable: "AccountingExpenseCategories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AccountingExpenseCattegoryManyRelation_AccountingExpenses_ExpenseId",
                        column: x => x.ExpenseId,
                        principalTable: "AccountingExpenses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AccountingInvoiceItems",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    AccountingCorrectionInvoiceId = table.Column<Guid>(nullable: true),
                    AccountingInvoiceId = table.Column<Guid>(nullable: true),
                    Amount = table.Column<int>(nullable: false),
                    GrossValue = table.Column<decimal>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    NetValue = table.Column<decimal>(nullable: false),
                    Pkuiw = table.Column<string>(nullable: true),
                    UnitValue = table.Column<decimal>(nullable: false),
                    VatValue = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccountingInvoiceItems", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AccountingInvoiceItems_AccountingCorrectionInvoices_AccountingCorrectionInvoiceId",
                        column: x => x.AccountingCorrectionInvoiceId,
                        principalTable: "AccountingCorrectionInvoices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AccountingInvoiceItems_AccountingInvoices_AccountingInvoiceId",
                        column: x => x.AccountingInvoiceId,
                        principalTable: "AccountingInvoices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AccountingRevenueCategoryManyRelation",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    RevenueCategoryId = table.Column<Guid>(nullable: false),
                    RevenueId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccountingRevenueCategoryManyRelation", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AccountingRevenueCategoryManyRelation_AccountingRevenueCategories_RevenueCategoryId",
                        column: x => x.RevenueCategoryId,
                        principalTable: "AccountingRevenueCategories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AccountingRevenueCategoryManyRelation_AccountingRevenues_RevenueId",
                        column: x => x.RevenueId,
                        principalTable: "AccountingRevenues",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AccountingCorrectionInvoices_CreatorUserId",
                table: "AccountingCorrectionInvoices",
                column: "CreatorUserId");

            migrationBuilder.CreateIndex(
                name: "IX_AccountingCorrectionInvoices_FarmId",
                table: "AccountingCorrectionInvoices",
                column: "FarmId");

            migrationBuilder.CreateIndex(
                name: "IX_AccountingCorrectionInvoices_InvoiceId",
                table: "AccountingCorrectionInvoices",
                column: "InvoiceId");

            migrationBuilder.CreateIndex(
                name: "IX_AccountingExpenses_CreatorUserId",
                table: "AccountingExpenses",
                column: "CreatorUserId");

            migrationBuilder.CreateIndex(
                name: "IX_AccountingExpenses_FarmId",
                table: "AccountingExpenses",
                column: "FarmId");

            migrationBuilder.CreateIndex(
                name: "IX_AccountingExpenses_PlannedExpenseId",
                table: "AccountingExpenses",
                column: "PlannedExpenseId");

            migrationBuilder.CreateIndex(
                name: "IX_AccountingExpenseCattegoryManyRelation_AccountingPlannedExpenseId",
                table: "AccountingExpenseCattegoryManyRelation",
                column: "AccountingPlannedExpenseId");

            migrationBuilder.CreateIndex(
                name: "IX_AccountingExpenseCattegoryManyRelation_ExpenseCategoryId",
                table: "AccountingExpenseCattegoryManyRelation",
                column: "ExpenseCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_AccountingExpenseCattegoryManyRelation_ExpenseId",
                table: "AccountingExpenseCattegoryManyRelation",
                column: "ExpenseId");

            migrationBuilder.CreateIndex(
                name: "IX_AccountingInfos_CreatorUserId",
                table: "AccountingInfos",
                column: "CreatorUserId");

            migrationBuilder.CreateIndex(
                name: "IX_AccountingInfos_FarmId",
                table: "AccountingInfos",
                column: "FarmId");

            migrationBuilder.CreateIndex(
                name: "IX_AccountingInvoices_AccountingInfoId",
                table: "AccountingInvoices",
                column: "AccountingInfoId");

            migrationBuilder.CreateIndex(
                name: "IX_AccountingInvoices_CreatorUserId",
                table: "AccountingInvoices",
                column: "CreatorUserId");

            migrationBuilder.CreateIndex(
                name: "IX_AccountingInvoices_FarmId",
                table: "AccountingInvoices",
                column: "FarmId");

            migrationBuilder.CreateIndex(
                name: "IX_AccountingInvoiceItems_AccountingCorrectionInvoiceId",
                table: "AccountingInvoiceItems",
                column: "AccountingCorrectionInvoiceId");

            migrationBuilder.CreateIndex(
                name: "IX_AccountingInvoiceItems_AccountingInvoiceId",
                table: "AccountingInvoiceItems",
                column: "AccountingInvoiceId");

            migrationBuilder.CreateIndex(
                name: "IX_AccountingPlannedExpense_CreatorUserId",
                table: "AccountingPlannedExpense",
                column: "CreatorUserId");

            migrationBuilder.CreateIndex(
                name: "IX_AccountingPlannedExpense_FarmId",
                table: "AccountingPlannedExpense",
                column: "FarmId");

            migrationBuilder.CreateIndex(
                name: "IX_AccountingPlannedRevenue_CreatorUserId",
                table: "AccountingPlannedRevenue",
                column: "CreatorUserId");

            migrationBuilder.CreateIndex(
                name: "IX_AccountingPlannedRevenue_FarmId",
                table: "AccountingPlannedRevenue",
                column: "FarmId");

            migrationBuilder.CreateIndex(
                name: "IX_AccountingRevenues_CreatorUserId",
                table: "AccountingRevenues",
                column: "CreatorUserId");

            migrationBuilder.CreateIndex(
                name: "IX_AccountingRevenues_FarmId",
                table: "AccountingRevenues",
                column: "FarmId");

            migrationBuilder.CreateIndex(
                name: "IX_AccountingRevenues_InvoiceId",
                table: "AccountingRevenues",
                column: "InvoiceId");

            migrationBuilder.CreateIndex(
                name: "IX_AccountingRevenues_PlannedRevenueId",
                table: "AccountingRevenues",
                column: "PlannedRevenueId");

            migrationBuilder.CreateIndex(
                name: "IX_AccountingRevenueCategoryManyRelation_RevenueCategoryId",
                table: "AccountingRevenueCategoryManyRelation",
                column: "RevenueCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_AccountingRevenueCategoryManyRelation_RevenueId",
                table: "AccountingRevenueCategoryManyRelation",
                column: "RevenueId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AccountingExpenseCattegoryManyRelation");

            migrationBuilder.DropTable(
                name: "AccountingInvoiceItems");

            migrationBuilder.DropTable(
                name: "AccountingRevenueCategoryManyRelation");

            migrationBuilder.DropTable(
                name: "AccountingExpenseCategories");

            migrationBuilder.DropTable(
                name: "AccountingExpenses");

            migrationBuilder.DropTable(
                name: "AccountingCorrectionInvoices");

            migrationBuilder.DropTable(
                name: "AccountingRevenueCategories");

            migrationBuilder.DropTable(
                name: "AccountingRevenues");

            migrationBuilder.DropTable(
                name: "AccountingPlannedExpense");

            migrationBuilder.DropTable(
                name: "AccountingInvoices");

            migrationBuilder.DropTable(
                name: "AccountingPlannedRevenue");

            migrationBuilder.DropTable(
                name: "AccountingInfos");
        }
    }
}
