﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FarmMachine.Api.Migrations
{
    public partial class ChangedFarmCategoryRelation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FarmCategories_Farms_FarmId",
                table: "FarmCategories");

            migrationBuilder.DropIndex(
                name: "IX_FarmCategories_FarmId",
                table: "FarmCategories");

            migrationBuilder.DropColumn(
                name: "FarmId",
                table: "FarmCategories");

            migrationBuilder.DropColumn(
                name: "OwnerUserId",
                table: "Farms");

            migrationBuilder.CreateTable(
                name: "FarmCategoryManyRelation",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    FarmCategoryId = table.Column<Guid>(nullable: false),
                    FarmId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FarmCategoryManyRelation", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FarmCategoryManyRelation_FarmCategories_FarmCategoryId",
                        column: x => x.FarmCategoryId,
                        principalTable: "FarmCategories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_FarmCategoryManyRelation_Farms_FarmId",
                        column: x => x.FarmId,
                        principalTable: "Farms",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_FarmCategoryManyRelation_FarmCategoryId",
                table: "FarmCategoryManyRelation",
                column: "FarmCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_FarmCategoryManyRelation_FarmId",
                table: "FarmCategoryManyRelation",
                column: "FarmId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "FarmCategoryManyRelation");

            migrationBuilder.AddColumn<Guid>(
                name: "FarmId",
                table: "FarmCategories",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OwnerUserId",
                table: "Farms",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_FarmCategories_FarmId",
                table: "FarmCategories",
                column: "FarmId");

            migrationBuilder.AddForeignKey(
                name: "FK_FarmCategories_Farms_FarmId",
                table: "FarmCategories",
                column: "FarmId",
                principalTable: "Farms",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
