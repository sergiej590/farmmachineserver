﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FarmMachine.Api.Migrations
{
    public partial class DeletedFarmAddressCategory : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FarmAddresses_FarmAddressCategories_CategoryId",
                table: "FarmAddresses");

            migrationBuilder.DropTable(
                name: "FarmAddressCategories");

            migrationBuilder.DropIndex(
                name: "IX_FarmAddresses_CategoryId",
                table: "FarmAddresses");

            migrationBuilder.DropColumn(
                name: "CategoryId",
                table: "FarmAddresses");

            migrationBuilder.AddColumn<bool>(
                name: "Main",
                table: "FarmAddresses",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Main",
                table: "FarmAddresses");

            migrationBuilder.AddColumn<Guid>(
                name: "CategoryId",
                table: "FarmAddresses",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateTable(
                name: "FarmAddressCategories",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Main = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FarmAddressCategories", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_FarmAddresses_CategoryId",
                table: "FarmAddresses",
                column: "CategoryId");

            migrationBuilder.AddForeignKey(
                name: "FK_FarmAddresses_FarmAddressCategories_CategoryId",
                table: "FarmAddresses",
                column: "CategoryId",
                principalTable: "FarmAddressCategories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
